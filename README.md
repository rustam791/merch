# Карта мерч
## Запуск проекта

* Сбилдить стили и js 
    ** npm run build
* Запустить проект
    ** через pm2(или анологичный менеджер)
    *** Для pm2 - pm2 start ecosystem.config.js

## Запуск проекта (dev)

* Отслеживание изменений
    **npm run watch
* Запуск проекта 
    ** npm run start