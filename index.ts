import { MainRender } from './lib/mainrender';
import { onClickWindow, openPopup, changeFileLabel, selectOption, toggleDropdownByClass, closeAllPopup } from './lib/ui';

window.onload = () => {
    // render canvas;
    const mainRender = new MainRender();
    Object.defineProperty(window, 'conva', {
        value: mainRender.CANVAS
    });

    Object.defineProperty(
        window, 
        'openPopup', {
            value: openPopup,
            configurable: true
        }
    );
    Object.defineProperty(
        window, 
        'changeFileLabel', {
            value: changeFileLabel,
            configurable: true
        }
    );
    Object.defineProperty(
        window, 
        'selectOption', {
            value: selectOption,
            configurable: true
        }
    );
    Object.defineProperty(
        window, 
        'toggleDropdownByClass', {
            value: toggleDropdownByClass,
            configurable: true
        }
    );
    Object.defineProperty(
        window, 
        'closeAllPopup', {
            value: closeAllPopup,
            configurable: true
        }
    );

    window.onclick = onClickWindow;
}



