export interface Calculate {
    height: number,
    rotate: number,
    width: number,
    x: number,
    y: number,
    vect?: any,
    id: string,
    name?: string
}

export interface Point {
    x:number,
    y:number
}
