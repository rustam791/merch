export interface Cell {
    AW_ID?: number,
    AX_ID?: string,
    ALTER_AX_ID?: string,
    AX_TAB_ID?: string,
    AX_REC_ID?: number,
    AX_DATA_AREA_ID?: string,
    AX_MODDIF_DATE?: string,
    AX_CREATE_DATE?: string,
    DESCRIPTION?: string,
    LOCATION_POSITION?: number,
    LOCATION_LEVEL?: number,
    LOCATION_RACK?: number,
    LOCATION_SORT?: number,
    INVENT_LOCATION_ID?: number,
    ANGLE?: number,
    HEIGHT?: number,
    M_X?: number,
    M_Y?: number,
    NAME?: string,
    TYPE_ID?: number,
    WIDTH?: number,
    WMS_ID?: number,
    DIVISION?: any,
    TYPE_CELL?: string
    TYPE_NAME?: groupClass
}


export interface GroupCell {
    AW_ID?: number,
    AX_ID?: string,
    ALTER_AX_ID?: string,
    AX_TAB_ID?: string,
    AX_REC_ID?: number,
    AX_DATA_AREA_ID?: string,
    AX_MODDIF_DATE?: string,
    AX_CREATE_DATE?: string,
    DESCRIPTION?: string,
    LOCATION_POSITION?: number,
    LOCATION_LEVEL?: number,
    LOCATION_RACK?: number,
    LOCATION_SORT?: number,
    INVENT_LOCATION_ID?: number,
    ANGLE?: number,
    HEIGHT?: number,
    M_X?: number,
    M_Y?: number,
    NAME?: string,
    TYPE_ID?: number,
    WIDTH?: number,
    WMS_ID?: number,
    DIVISION?: any,
    TYPE_CELL?: string
}


export interface CellStartRender {
    AX_CREATE_DATE: string,
    AX_ID: string,
    AX_MODDIF_DATE: string,
    AX_REC_ID: number,
    ID: number,
    INVENT_LOCATION_ID: number,
    DIVISION: any,
    TYPE_CELL: string
}


export interface ShopsSearchItems {
    L_JSON: CellsSearchItems[];
    DIVISION_ID: number;
    NAME: string;
    WMS_AX_ID_STR: string;
}

export interface CellsSearchItems {
    ID: number,
    AX_ID: string
}


export interface TypeItems {
    AX_ID: string;
    ID: number;
    NAME: string;
}


export interface Producer {
    ID: number;
    NAME: string;
    
}

export interface Manager {
    NAME: string;
    ID: number
}

export interface Collection {
    NAME: string;
    AX_ID:string;
    ID: number; 
}


export type groupClass = 'кулла' | 'стенд книжка' | 'стенд напольный' | 'стенд' | 'default'


