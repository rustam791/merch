export interface ItemProduct {
    ARTICLE: string;
    AX_REC_ID: number;
    ID: number;
    NAME: string;
    PATH: string;
    TOTAL: number;
}