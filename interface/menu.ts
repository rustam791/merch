export interface Menu {
    AX_REC_ID: number;
    NAME: string;
    DIVISION_ID: number;
    FLOOR: number;
    ID: number;
    WMS_AX_ID_STR: string
}