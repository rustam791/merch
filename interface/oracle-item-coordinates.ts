import { Point } from './calculate'

export interface OracleItemCoordinates {
    uniqueName: string;
    originPoint: Point;
    width: number;
    height: number;
    angle: number;
}