import Konva from 'konva';

export interface Prototype {
    
}

export interface CallBackObj {
    event: string;
    // func: Konva.KonvaEventListener<any, any>
    func: any;
    http?: any;
}