import { Cell, groupClass } from "../interface/cell";

export function ArrayBuildParse(target: Object, properyKey: string) {
    let _value: any;
    const getter = () => {
        return _value;
    };
    const setter = (newVal: Array<Cell>) => {
        _value = newVal.map(val => {
            val.TYPE_NAME = <groupClass>val.TYPE_NAME?.replace(/[\s0-9]+$/, '')
            return val;
        }).filter(val => val.TYPE_NAME);
    };
    Object.defineProperty(target, properyKey, {
        get: getter,
        set: setter
    });
}

