import { ungzip } from 'pako';
import { ajax, AjaxRequest, AjaxResponse } from 'rxjs/ajax'
import { Observable, } from 'rxjs/internal/Observable';
import { catchError, finalize, map, mergeMap, observeOn, subscribeOn, tap } from 'rxjs/operators';
import { Menu } from '../interface/menu';
import { Cell, Collection } from '../interface/cell';
import { json } from 'body-parser';
import { merge, animationFrameScheduler, asapScheduler, asyncScheduler, of, queueScheduler, scheduled, Subscriber } from 'rxjs';



const { Subject } = require('rxjs');
// const { merge } = require('rxjs/operators');

const URL_DOMIN = 'http://127.0.0.1:8000/merch/';
const URL_AOTORIZED = 'http://127.0.0.1:8000/api-token-auth/';
// const URL_DOMIN = 'http://192.168.9.41:4333/merch/';
const URL_DOMIN_SAVE_IMAGE = ''

const DELT_UPDATE_TOKEN = 5 * 60 * 1000  // в милисекундах


export class HttpApi {




    poolRequest: Array<number> = [];

    tokenTime: number;

    constructor() {


    };
    encoding(zipData: string) {
        return JSON.parse(window.atob(zipData));
    }

    autorized(inputs: Object) {
        return ajax({
            method: 'POST',
            url: URL_AOTORIZED,
            body: inputs,
            headers: {
                'Content-Type': 'application/json; utf-8'
            }
        })
            .pipe(map(($response: AjaxResponse) => {
                return $response?.response;
            }))
    }


    removeNotValidAutorizedData(): void {
        sessionStorage.clear();
        localStorage.clear();
    }

    ajaxIntercept(urlOrRequest: AjaxRequest): Observable<any> {
        if (!this.avtorized()) {
            this.removeNotValidAutorizedData();
            throw new Error('401');
        }

        this.poolRequest.push(this.poolRequest.length);         // preloader start 
        document.querySelector('html').style.cursor = 'progress';
        document.querySelector('html').style.pointerEvents = 'none';
        urlOrRequest.headers = {
            'Authorization': `JWT ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json; utf-8'
        }


        return ajax(urlOrRequest)
            .pipe(
                finalize(() => {
                    // if(this.poolRequest[this.poolRequest.length] !== this.poolRequest.shift())
                    this.poolRequest.shift();
                    if (!this.poolRequest.length) {   // preloader end 
                        document.querySelector('html').style.cursor = 'auto';       // high level 
                        document.querySelector('html').style.pointerEvents = 'all'
                    }
                }),
                catchError(err => {
                    console.log(err);
                    if (err.status === 401) {
                        this.removeNotValidAutorizedData();
                        this.openPopupFormAutorized();
                    }
                    throw new Error('не обработанная ошибка!');
                    // return err
                }))

    }

    avtorized(): Boolean {
        if (!localStorage.getItem('token')) return false;
        const TOKEN_TIME = JSON.parse(window.atob(localStorage.getItem('token').split('.')[1])).orig_iat * 1000;  // парсим вторую часть токена для просмотра времени
        return TOKEN_TIME && (new Date()).getTime() - TOKEN_TIME * 1000 < DELT_UPDATE_TOKEN                        // проверка осталось 5 минут до конца жизни токена
    }
    openPopupFormAutorized() {
        document.querySelector('.popup.auth_shop').classList.add('open');
    }


    // checkToken(res: AjaxResponse): Observable<any> {
    //     // return res.status === 403 ? empty() : this.ajaxIntercept({

    //     // })
    //     return of()
    // }

    savePlanImage(image: File, fileName: string): Observable<any> {
        const formData = new FormData();
        formData.append('file', image);
        formData.append('fileName', fileName);
        return ajax({
            method: 'POST',
            url: `/exist_image_plan`,
            body: { fileName },
        }).pipe(
            mergeMap((res: AjaxResponse) =>
                res?.response?.check ? new Observable() : this.saveFile(formData) // проверка на существование файла с таким именем и далее если есть, то пропуск, иначе - сохранить
            ),
            mergeMap((resp: AjaxResponse) =>
                this.pushPathFile2DB(resp?.response?.name, fileName?.split('.')[0])  // сохранение название файла в БД(название сгенерированное express(бек сервер))
                // это сделано для отслеживания истории загрузки планов
            )
        );
    }

    saveFile(formData: FormData): Observable<Object> {
        return ajax({
            method: 'POST',
            url: `/save_image`,
            body: formData,
        })
    }

    pushPathFile2DB(FILE_NAME: string, AX_ID: string): Observable<Object> { 
        const formData = {AX_ID: AX_ID, FILE_NAME: FILE_NAME};
        return this.ajaxIntercept({
            method: 'POST',
            url: `${URL_DOMIN}add_plan/`,
            body: formData
        })
    }

    /**
     * @param  {Cell[]} cells
     * @returns void
     */
    saveArray(cells: Cell[]): void {
        this.ajaxIntercept({
            method: 'POST',
            url: `${URL_DOMIN}save_pallets/`,
            body: {
                cells: JSON.stringify(cells.map(val => {
                    val.DIVISION = sessionStorage.getItem('DIV_ID') || (<HTMLOrSVGElement><Object>document.querySelector('.menu-item'))?.dataset?.div_id;
                    return val;
                }))
            }
        })
            .subscribe(
                status => console.log(status),
                err => console.log(err)
            );
    }
    /**
     * @param  {Cell[]} cells
     * @returns void
     */
    updateArray(cells: Cell[]): void {
        this.ajaxIntercept({
            method: 'POST',
            url: `${URL_DOMIN}update_cells/`,
            body:
            {
                cells: JSON.stringify(cells.map(val => {
                    val.DIVISION = sessionStorage.getItem('DIV_ID') || (<HTMLOrSVGElement><Object>document.querySelector('.menu-item'))?.dataset?.div_id;
                    return val;
                }))
            }
        }).subscribe(status => console.log, err => console.log);
    }
    /**
     * @param  {Cell[]} cells
     * @returns void
     */
    deleteArray(cells: Cell[]): void {
        this.ajaxIntercept({
            method: 'POST',
            url: `${URL_DOMIN}delete_cells/`,
            body: {
                cells: JSON.stringify(cells)
            }
        })
            .subscribe(status => console.log, err => console.log);
    }
    /**
     * @returns Observable
     */
    getListMenu(): Observable<Menu[]> {

        return this.ajaxIntercept({
            method: "GET",
            url: `${URL_DOMIN}get_menu/`
        })
            .pipe(map(($response: AjaxResponse) =>  $response.status === 401 ? $response : $response.response))
    }
    
    /**
     * @param  {number} id
     * @returns Observable
     */
    getArrayCells(div_id: string, ax_id: string): Observable<Cell[]> {
        return this.ajaxIntercept({ method: 'GET', url: `${URL_DOMIN}get_cells/?DIV_ID=${div_id}&AX_ID=${ax_id}` })
            .pipe(map(($response: AjaxResponse) =>
                $response.response ? JSON.parse($response.response) : $response.response
            ))
    }

    /**
     * @param  {number} id
     * @returns Observable
     */
     getArrayCellsSlaves(div_id: string, ax_id: string): Observable<Cell[]> {
        return this.ajaxIntercept({ method: 'GET', url: `${URL_DOMIN}get_cells_slaves/?DIV_ID=${div_id}&AX_ID=${ax_id}` })
            .pipe(map(($response: AjaxResponse) =>
                $response.response ? JSON.parse($response.response) : $response.response
            ))
    }

    /**
     * @param  {number} id
     * @returns Observable
     */
    getArrayCellsCanvas(div_id: string, ax_id: string): Observable<Cell[]> {
        return this.ajaxIntercept({
            method: 'GET',
            url: `${URL_DOMIN}get_cells_canvas/?DIV_ID=${div_id}&AX_ID=${ax_id}`
        })
            .pipe(map(($response: AjaxResponse) =>
                $response.response ? JSON.parse($response.response) : $response.response
            ))
    }


    getPlanPathImage(AX_ID: string): Observable<any> {
        // в наличии
        // акция
        // новинка
        // под заказ
        // остатки
        // клондайк
        // мусор
        // APP_ITEMSMATERIALTYPE(ID) -> APP_ITEMS(MATERIAL_TYPE_ID)
        return this.ajaxIntercept({
            method: 'POST',
            url: `${URL_DOMIN}get_plan_image/`,

            body: { AX_ID }
        })
            .pipe(map(res => JSON.parse(res.response)))   // # говнокод - нет интерфейса
    }

    getDataCell(ID: number, DIVISION_ID: Number): Observable<any> {       //# говнокод - нет интерфейса
        return this.ajaxIntercept({
            method: 'POST',
            url: `${URL_DOMIN}get_data_cell/`,
            body: {
                ID,
                DIVISION_ID
            }
        })
            .pipe(map(($response: AjaxResponse) => $response.response ? $response.response : $response.response))
    }

    /**
     * @param  {string} searchLine
     * @returns Observable with array plans have searching cell(прошу прощения за мой ангельский)
     * Переход сразу к ячейке, подсветка, зум и т.д.
     */
    searchAllCellsLike(
        AX_ID: string,               // name-db
        MATERIAL_TYPE_ID?: number[], // name-db
        TOTAL?: number,            //
        MANAGERS?: number[],         // 
        CELL_ID?: string,            //   
        PRODUCER_ID?: number,        // name-db
        COLLECTION_ID?: number       // name-db
    ): Observable<any> {         // # говнокод - нет интерфейса           
        const APP_ITEMS_ARRAY = JSON.stringify({
            AX_ID: AX_ID || 0,
            PRODUCER_ID: PRODUCER_ID || 0,
            COLLECTION_ID: COLLECTION_ID || 0
        });
        const APP_WMS_TOTAL_TABLE_ARRAY = JSON.stringify({
            TOTAL: TOTAL || 0,
            CELL_ID: CELL_ID || 0
        });
        const APP_ARRAY_TYPE_AND_MANAGER = JSON.stringify({
            MATERIAL_TYPE_ID: MATERIAL_TYPE_ID.length ? MATERIAL_TYPE_ID : 0,
            MANAGERS: MANAGERS.length ? MANAGERS : 0
        });

        return this.ajaxIntercept({
            method: 'POST',
            url: `${URL_DOMIN}search_item/`,
            body: {
                APP_ITEMS_ARRAY,
                APP_WMS_TOTAL_TABLE_ARRAY,
                APP_ARRAY_TYPE_AND_MANAGER
            }
        })
            .pipe(map(($response: AjaxResponse) =>
                $response.response ? JSON.parse($response.response) : $response.response
            ));
    }

    getCellsItemGroup(AX_ID: string, DIVISION_ID: string): Observable<any> {
        return this.ajaxIntercept({
            method: 'POST',
            url: `${URL_DOMIN}get_cells_item_group/`,
            body: {
                AX_ID,
                DIVISION_ID
            }
        })
            .pipe(map(($response: AjaxResponse) => $response.response ? JSON.parse($response.response) : $response.response))
    }

    getTypesMaterials(): Observable<any> {
        return this.ajaxIntercept({
            method: 'POST',
            url: `${URL_DOMIN}get_type_items_list/`
        })
            .pipe(map(($response: AjaxResponse) => $response.response ? JSON.parse($response.response) : $response.response))
    }

    getProducerItem(): Observable<any> {
        return this.ajaxIntercept({
            method: 'POST',
            url: `${URL_DOMIN}get_producer_items_list/`
        })
            .pipe(map(($response: AjaxResponse) => $response.response ? JSON.parse($response.response) : $response.response))
    }


    getCollectionItem(PRODUCER_ID: number): Observable<any> {
        return this.ajaxIntercept({
            method: 'POST',
            url: `${URL_DOMIN}get_collection_items_list/`,
            body: { PRODUCER_ID }
        })
            .pipe(map(($response: AjaxResponse) => $response.response ? (<Collection>JSON.parse($response.response)) : $response.response))
    }

    getManagers(): Observable<any> {
        return this.ajaxIntercept({
            method: 'POST',
            url: `${URL_DOMIN}get_managers/`
        })
            .pipe(map(($response: AjaxResponse) => $response.response ? JSON.parse($response.response) : $response.response))
    }


    // get_collection_items_list

    // searchMarkets4Product(AX_ID:string){}
}
