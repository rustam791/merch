import { forkJoin, Observable, Subscription } from 'rxjs';
import { ADMIN_GROUP, WorkField } from '../lib/workfield';
import { HttpApi } from '../lib/httpapi';
import { UIEvents } from '../lib/uievents';
import { Cell, TypeItems, Producer, Manager } from '../interface/cell';
import { Menu } from '../interface/menu';
import { finalize } from 'rxjs/operators';

export class MainRender {

    market = {
        container: 'container',
        width: 1000,
        height: 1000,
        imageSrc: './plan.png'
    };
    CANVAS: WorkField;
    API: HttpApi;
    UI: UIEvents;

    constructor() {
        this.API = new HttpApi();
        this.UI = new UIEvents();
        this.UI.http = this.API;

        let greatStream$: Subscription;
        this.UI.loadData = this.loadData;
        this.UI.autorizedUIevents();
        this.loadData()
    }

    loadData(): void {
        try {
            this.API.getListMenu()
                .pipe(finalize(() => {    // выполнить ПОСЛЕ подписки на получение меню
                    this.API.getPlanPathImage(
                        sessionStorage.getItem('AX_ID') ??
                        (<HTMLOrSVGElement><Object>document.querySelector('.menu-item'))?.dataset?.wms_ax_id_str
                    ).subscribe(
                        data => {
                            if (!data?.length) { return };
                            const PLAN = data.reduce((accumulate: any, current: any) => { // найти план этажа для загрузки на страницу
                                if (!accumulate) {
                                    return current
                                } else {
                                    return new Date(accumulate.DATE) > new Date(current.DATE) ? accumulate : current;
                                }
                            })
                            this.market.imageSrc = `/${PLAN?.URL}`;
                            this.startRenderDefaultPage();
                        },
                        err => console.log(err),
                        () => (
                            this.CANVAS = new WorkField(this.market),
                            this.UI.addEventLoadPlanFloors(),
                            JSON.parse(localStorage.getItem('ug')).includes(ADMIN_GROUP) ?
                                document.querySelectorAll('.admin')
                                    .forEach(val => val.classList.remove('admin')) :
                                {}
                        ))
                }))
                .subscribe(data => {
                    // console.log(data);
                    this.UI.renderMenu(<Menu[]>data, this.CANVAS);
                    this.UI.createlistImage(data);

                }, err => console.log(err),
                    () => { // по Завершению загрузки меню запросить план
                    }
                )
        } catch (error) {
            // greatStream$?.unsubscribe();
            console.log(error)
            if (error.message === '401') {
                this.API.openPopupFormAutorized();
            }
        }
    }

    startRenderDefaultPage(): void {
        // const arrayFunctionPrerender = this.UI.buildArr();

        const forkStream$: Subscription = forkJoin(this.UI.buildArr()).subscribe((data) => {      // render page
            
            
            this.UI.renderListSelect(data[1], this.CANVAS.newOption)                    // # говнокод необходим интерфейс пробрасывается any // render datalist 

            this.UI.arrayCells = <Cell[]>data[1];                                       // add array NOT canvas cells in const
            this.UI.addMenuEvents(this.CANVAS);

            this.UI.addEvent2Tabs(this.CANVAS);
            this.UI.addEvenetSave2Db(
                this.CANVAS
            );


            // Params is array string. 
            // Filter array convas rect and Group add event (send to DB array cells) 
            // forkStream$.unsubscribe();
            this.UI.renderCanvasCells(<Cell[]>data[0], this.CANVAS);                    // render canvas cells
            this.UI.addListenerSearchInput(this.CANVAS);
            // this.UI.addEvent2SelectSearchItems(this.CANVAS);
            this.UI.renderSearchTypeCheckbox(<TypeItems[]>data[2]);

            this.UI.renderDataListProducer(<Producer[]>data[3]);
            this.UI.renderManagers(<Manager[]>data[4]);
            this.UI.addEventInputFilter();




            Object.defineProperty(
                window,
                'convas', {
                value: this.CANVAS,
                configurable: true
            }
            );
        }, err => console.error(err)
        // , () => forkStream$.unsubscribe()
        );

    }




}

