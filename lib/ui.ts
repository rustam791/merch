// add shops popup

export function closeAllPopup() {
    let popupList = document.querySelectorAll('.popup');
    popupList.forEach(el => {
        if (el.classList.contains('open')) el.classList.remove('open')
    });
}
export function openPopup (name: string) {
    let popup = document.querySelector('.popup.' + name);
    if (!popup.classList.contains('open')) popup.classList.add('open')
}
export function toggleDropdownByClass(className: string) {
    let dropdownHeader = document.querySelector('.dropdown.' + className).querySelector('.dropdown_header');
    let dropdownBody =  document.querySelector('.dropdown.' + className).querySelector('.dropdown_body');
    dropdownHeader.classList.toggle('open');
    dropdownBody.classList.toggle('open');
    // document.querySelector('#selected_level').classList.remove('not_valid');
}
// close all dropdowns
export function onClickWindow(e: any) {
        
    if (!e.target.matches('.dropdown_header') && !e.target.closest('.dropdown_header')) {

        let dropdownList = document.querySelectorAll('.dropdown');

        dropdownList.forEach(el => {
            if(el.querySelector('.dropdown_header').matches('.open')) el.querySelector('.dropdown_header').classList.remove('open');
            if(el.querySelector('.dropdown_body').matches('.open')) el.querySelector('.dropdown_body').classList.remove('open');
        })
    }
}
export function selectOption(elem: any){
    let optionValue = elem.target.textContent;
    let dropdownHeader = elem.target.parentNode.parentNode.parentNode.querySelector('.dropdown_header');
    dropdownHeader.querySelector('.text').textContent = optionValue;
    dropdownHeader.classList.add('selected'); 
    document.querySelector('#selected_level').classList.remove('not_valid');
}
export function changeFileLabel(elem: any) {
    let fileName = elem.target.value.replace(/.*\\/, "");
    let labelNode = elem.target.parentNode.querySelector('label');
    
    let defaultLabelVal = labelNode.textContent;

    if (fileName) {
        labelNode.textContent = fileName;
        labelNode.classList.add('selected');
    } else {
        labelNode.textContent = defaultLabelVal;
        if (labelNode.classList.contains('selected')) labelNode.classList.remove('selected');
    }
    
}
// document.querySelector('.custom_file_input').value.replace(/.*\\/, "")
