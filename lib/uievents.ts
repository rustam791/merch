import { Cell, GroupCell, CellStartRender, CellsSearchItems, ShopsSearchItems, TypeItems, Producer, Manager, Collection, groupClass } from '../interface/cell';
import { WorkField, ID_GROUP, ID_ONCE } from './workfield';
import { HttpApi } from './httpapi';
import Konva from 'konva';
import { Menu } from '../interface/menu';
import { Observable, forkJoin } from 'rxjs';
import { closeAllPopup, selectOption } from './ui';
import { ItemProduct } from '../interface/item_product';
// import { ApiError } from 'next/dist/next-server/server/api-utils';
import { finalize } from 'rxjs/operators';
import { Group } from 'konva/types/Group';
import { ArrayBuildParse } from './decorators';
// import { filter } from 'rxjs/operators';
export const IMAGE_HOSTNAME = 'https://new.terraceramica.ru/document/?image='


export class UIEvents {

    @ArrayBuildParse
    arrayCells: Cell[];
    arrayCellsOnce: Cell[];
    http: HttpApi;
    arrayFilterProducers: Array<Producer>;
    arrayCollectionProducers: Array<Collection>;
    market = {
        container: 'container',
        width: 1000,
        height: 1000,
        imageSrc: './plan.png'
    };
    loadData: () => void;


    // getDataCellFunctions(): Function {
    //     const BUILD_ITEM = this.buildItem,
    //         HTTP = this.http;
    //     return () => {
    //         const WMS_ID = +(<HTMLInputElement>document.querySelector('#search_nomenclat_cells_work')).dataset.id;
    //         HTTP.getDataCell(
    //             WMS_ID,
    //             +(sessionStorage.getItem('DIV_ID') || (<HTMLOrSVGElement><Object>document.querySelector('.menu-item'))?.dataset?.div_id)
    //         ).subscribe(data => {
    //             console.log(WMS_ID)
    //             let objectList = JSON.parse(data);
    //             const wrapper: HTMLLIElement = document.querySelector('#json_data');
    //             BUILD_ITEM(objectList, wrapper, );
    //         })
    //     }
    // }


    /**
     * @param  {WorkField} canvas
     * @returns void
     * add Events to document
     */
    addEvent2Tabs(canvas: WorkField): void { 
        document.getElementById('group').addEventListener('click', () => {
            // const cell: Cell = {}
            const AX_ID = (<HTMLInputElement>document.querySelector('#search_nomenclat_cells_work')).value;
            const WMS_ID = +(<HTMLInputElement>document.querySelector('#search_nomenclat_cells_work')).dataset.id;
            if (!AX_ID) return this.notSelectedError();
            (<HTMLInputElement>document.querySelector('#search_nomenclat_cells_work')).value = '';
            canvas.factory(this.arrayCells.filter(cell => cell.WMS_ID === WMS_ID)[0], this.arrayCellsOnce.filter(val => val.AX_ID.includes(`${AX_ID}-`)),
                {
                    event: 'click',
                    func: (e: Event) => {
                        this.http.getDataCell(
                            (<Konva.Rect><any>e.target).attrs.WMS_ID,
                            +(sessionStorage.getItem('DIV_ID') || (<HTMLOrSVGElement><Object>document.querySelector('.menu-item'))?.dataset?.div_id)).subscribe(data => {
                                let objectList = JSON.parse(data);
                                const wrapper: HTMLLIElement = document.querySelector('#json_data');
                                this.buildItem(objectList, wrapper, (<Konva.Rect><any>e.target).attrs.id);
                            })
                    }
                });
            document.getElementById(AX_ID).remove();
        });
    }

    addEventLoadPlanFloors(): void {
        document.querySelector('#test_load_file_button').addEventListener('click', async e => {
            e.preventDefault();
            e.stopPropagation();
            if ((<HTMLInputElement>document.querySelector('#selected_level')).classList.contains('not_valid')) { return };
            let fileName = (<HTMLInputElement>document.querySelector('#selected_level')).textContent;
            let file = (<HTMLInputElement>document.querySelector('#test_load_file_input')).files.item(0);
            fileName += file.name.slice(file.name.indexOf('.', file.name.length - 4), file.name.length);
            this.http.savePlanImage(file, fileName).subscribe(resp => {
                (<HTMLInputElement>document.querySelector('#popup_set_level div.text')).textContent = 'Выберите этаж';
                document.querySelector('#popup_set_level .dropdown_header.selected').classList.remove('selected');
                (<HTMLLabelElement>document.querySelector('#popup_set_level .upload_file label')).textContent = 'Выберите файл...';
                document.querySelector('#popup_set_level .upload_file label').classList.remove('selected');
            });
        });
    }

    /**
     * @param  {HttpApi} rx
     * @param  {Array<string>} cells
     * @returns void
     * Add event button save array cells
     * Use rx.ajax void function save array.
     */
    addEvenetSave2Db(CANVAS: WorkField): void {
        document.querySelector('#save').addEventListener('click', e => {
            const cellsInsertArray: Cell[] = [];
            const cellsUpdate: Cell[] = [];
            const cellsDeleteArray: any = [];
            document.querySelectorAll('#lsit_cells>.list_item').forEach((val: HTMLDivElement) => {
                cellsDeleteArray.push({
                    WMS_ID: val.dataset.id
                });
            });
            CANVAS
                .arrayRect
                .map((cell: Konva.Group) => {
                    if (cell.attrs.check) {
                        cellsInsertArray.push(Object({
                            NAME: cell.name(),
                            DESCRIPTION: '',
                            ANGLE: cell.rotation(),
                            M_X: cell.x(),
                            M_Y: cell.y(),
                            TYPE_ID: cell instanceof Konva.Group ? ID_GROUP : ID_ONCE,                // hard code
                            WMS_ID: cell.attrs.WMS_ID, // id cell
                            HEIGHT: cell.height(),
                            WIDTH: cell.width()
                        }));
                        cell.attrs.check = false;
                    } else {
                        cellsUpdate.push(Object({
                            NAME: cell.name(),
                            DESCRIPTION: '',
                            ANGLE: cell.rotation(),
                            M_X: cell.x(),
                            M_Y: cell.y(),
                            TYPE_ID: cell instanceof Konva.Group ? ID_GROUP : ID_ONCE,                  // hard code
                            WMS_ID: cell.attrs.WMS_ID, // id cell
                            HEIGHT: cell.height(),
                            WIDTH: cell.width()
                        }))
                    }
                    return cell;
                });
            try {
                cellsInsertArray.length ? this.http.saveArray(cellsInsertArray) : {};
            } catch (error) {
                console.log(error);
            }
            try {
                cellsUpdate.length ? this.http.updateArray(cellsUpdate) : {};
            } catch (error) {
                console.log(error);
            }
            try {
                cellsDeleteArray.length ? this.http.deleteArray(cellsDeleteArray) : {};
            } catch (error) {
                console.log(error);
            }
        });
    }


    /**
     * @param  {WorkField} CANVAS
     * @returns void
     */
    addEvent2SelectSearchItems(elementNode: Node ,CANVAS: WorkField): void {
        console.log('1');
        
        
        elementNode.addEventListener('click', (e) => {
            console.log(e.target);
            
            const EV_DIV = <HTMLDivElement>e.target;

            const arrayShops = (<ShopsSearchItems[]>JSON.parse(sessionStorage.getItem('cells-seach-items')))                         // find and return 
                .filter(shop => (shop.DIVISION_ID === +EV_DIV.dataset.div_id && shop.WMS_AX_ID_STR === EV_DIV.dataset.wms_ax_id_str))[0];  // changed market/shop/stok

            const arrayCells: Array<Konva.Group> = [];
            sessionStorage.setItem('AX_ID', EV_DIV.dataset.wms_ax_id_str);
            sessionStorage.setItem('DIV_ID', EV_DIV.dataset.div_id);
            this.http.getPlanPathImage(sessionStorage.getItem('AX_ID')).subscribe(data => {
                if ((!data || !data.length) || !data?.length) return;
                // if () return;
                if (data.length === 1) {
                    let imageSrc = `/${data[0].URL}`;
                    this.market = {
                        container: 'container',
                        width: 1000,
                        height: 1000,
                        imageSrc: imageSrc
                    }
                    this.clearPage(CANVAS);
                    
                    return;
                }
                const PLAN = data.reduce((accumulate: any, current: any) => { // найти план этажа для загрузки на страницу
                    if (!accumulate) {
                        return current
                    } else {
                        return new Date(accumulate.DATE) > new Date(current.DATE) ? accumulate : current
                    }
                })

                const imageSrc = `/${PLAN?.URL}`;
                this.market = {
                    container: 'container',
                    width: 1000,
                    height: 1000,
                    imageSrc: imageSrc
                }
                this.clearPageSubscriber(CANVAS).subscribe(async data => {
                    await this.renderCanvasCells(<Cell[]>data[0], CANVAS, true);                  // render canvas cells
                    this.renderListSelect(data[1], CANVAS.newOption)                  // # говнокод необходим интерфейс пробрасывается any // render datalist 
                    this.arrayCells = <Cell[]>data[1];
                    CANVAS.arrayRect.forEach((cell: Group) => {
                        for (const cellShop of arrayShops.L_JSON) {
                            cell.children.each((cellRect: Konva.Rect) => {
                                if (cellRect.id() === cellShop.AX_ID) {
                                    this.drawElement(cellRect, '#ff0000', [true]);
                                }
                            });
                        }
                    });
                    CANVAS.layer.draw();
                    closeAllPopup()
                    // }, 1000);
                    // CANVAS.zoom(arrayCells);

                }, err => closeAllPopup());
            });
        })
    }
    /**
     * @returns void
     * information function
     */
    notSelectedError(): void {
        alert("Not Use Select Option")
    }
    /**
     * @param  {Cell[]} cells
     * Add option cells to datalist. Сбор массива ячеек.
     */
    // renderListSelect(cells: CellStartRender[] | Cell[], addMewOption: Function): void {
    renderListSelect(cells: Array<Cell | GroupCell>, addMewOption: Function): void {
        // let arrayGR = cells.filter((val: any) => val.TYPE_CELL === 'ГРУППА').map((val: any) => val.AX_ID);
        this.arrayCellsOnce = cells.filter(cell => cell.TYPE_CELL === 'ЕДИНИЦА'); // фильтрация только единицы

        // this.arrayCellsOnce = cells.filter((cell: Cell | GroupCell) => cell.TYPE_CELL === 'ЕДИНИЦА') // фильтрация только единицы
        // .filter((cell: Cell | GroupCell) => {
        //     if(cell.TYPE_CELL !== 'ЕДИНИЦА') return false;
        //     return arrayGR.filter((cellGR: Cell) => cell.AX_ID.indexOf('cellGR') + 1).length
        // }); //добавить  если есть родитель

        // let arrayCellsOnce = cells
        //     .filter((val: any) => val.TYPE_CELL === 'ЕДИНИЦА')
        //     .filter((cell: any) => !arrayGR.filter((cellGR: any) => cell.AX_ID.indexOf(cellGR) + 1).length);
        [...cells.filter((val: Cell) => val.TYPE_CELL === 'ГРУППА')]?.map((cell: any) => {

            addMewOption(cell);
        });
    }




    buildDBCellsMap(cell: Cell, canvas: WorkField): void {
        const DIVISION_ID = +(sessionStorage.getItem('DIV_ID') || (<HTMLOrSVGElement><Object>document.querySelector('.menu-item'))?.dataset?.div_id);
        canvas.addRect(
            cell.AX_ID,
            cell.WMS_ID,
            cell,
            {
                event: 'click',
                func: () => {
                    this.http.getDataCell(cell.WMS_ID, DIVISION_ID).subscribe(data => {
                        let objectList = JSON.parse(data);
                        const wrapper: HTMLLIElement = document.querySelector('#json_data');
                        this.buildItem(objectList, wrapper, cell.AX_ID)
                    })
                }
            }
        );
    }

    async buildGroup(group: Cell, canvas: WorkField) {
        let arrayRect = 
        await (new Promise(async res => {
            await this.http.getArrayCells(
                sessionStorage.getItem('DIV_ID') ??
                (<HTMLOrSVGElement><Object>document.querySelector('.menu-item'))?.dataset?.div_id,
                `${group.AX_ID}-`)
                .subscribe(async arrayCells => {
                    arrayRect = await canvas.factory(group, arrayCells,
                        {
                            event: 'click',
                            func: (e: Event) => {
                                this.http.getDataCell(
                                    (<Konva.Rect><any>e.target).attrs.WMS_ID,
                                    +(sessionStorage.getItem('DIV_ID') || (<HTMLOrSVGElement><Object>document.querySelector('.menu-item'))?.dataset?.div_id)).subscribe(data => {
                                        let objectList = JSON.parse(data);
                                        const wrapper: HTMLLIElement = document.querySelector('#json_data');
                                        this.buildItem(objectList, wrapper, (<Konva.Rect><any>e.target).attrs.id)
                                    })
                            }
                        });
                    res(arrayRect)
                });
        }))
        return arrayRect;
        
        // canvas.addGroup(id, wms_id, this.arrayCellsOnce.filter(val => val.AX_ID.includes(id)));
    }


    async loadCells(iter: Array<Cell>, canvas: WorkField) {
        return Promise.all(iter.map(group => this.buildGroup(group, canvas)))
    }

    async renderCanvasCells(cells: Cell[], canvas: WorkField, search = false) {
        let groups = cells.filter(group => group.TYPE_ID === ID_GROUP).map(val => {
            val.TYPE_NAME = <groupClass>val.TYPE_NAME.replace(/[\s0-9]+$/, '');
            return val;
        });   //
        const cellFilter = cells.filter(cell => cell.TYPE_ID === ID_ONCE);  //        
        let a = await this.loadCells(groups, canvas);
        console.log(a);
        
        // if(search) {
        //     for (const group of groups) {
        //         await this.buildGroup(group, canvas);
        //     }
        // } else {
        //     for (const group of groups) {
        //         this.buildGroup(group, canvas);
        //     }
        // }
        
        console.log(canvas);
        console.log(canvas.arrayRect);

        cellFilter?.map(cell => this.buildDBCellsMap(cell, canvas));        //
    }
    renderMenu(arrayMeny: Menu[], canvas: WorkField): void {
        const ul = document.createElement('ul');
        arrayMeny?.map(itemMenu => {
            if (!(itemMenu.WMS_AX_ID_STR.indexOf('vs') + 1)) return;
            const li = document.createElement('li');
            const span = document.createElement('span'); // only alpha version
            const spanText = document.createElement('span');
            span.textContent = `  ==>  ${itemMenu.WMS_AX_ID_STR}`;
            spanText.textContent = itemMenu.NAME;
            spanText.classList.add('text_market');
            li.setAttribute('data-floor', '' + itemMenu.FLOOR);
            li.setAttribute('data-id', '' + itemMenu.ID);
            li.setAttribute('data-div_id', '' + itemMenu.DIVISION_ID);
            li.setAttribute('data-wms_ax_id_str', '' + itemMenu.WMS_AX_ID_STR);
            li.setAttribute('class', 'menu-item');
            li.appendChild(spanText);
            li.appendChild(span);
            ul.appendChild(li);

        });
        document.querySelector('#menu').appendChild(ul);
        // this.addMenuEvents(canvas);
    }
    /**
     * Function - add listenner 2 input search cells
     * @param  {WorkField} CANVAS
     * @returns void
     */
    addListenerSearchInput(CANVAS: WorkField): void {
        document.querySelector('#search_cells').addEventListener('keyup', e => {
            if (!(<HTMLInputElement>e.target).value) {
                // CANVAS.arrayRect.map(cell => (this.drawElement(cell, '#808080', [true]), cell));
                CANVAS.arrayRect.map((gr: Konva.Group) =>
                    gr.children.each((cell: Konva.Rect) => (this.drawElement(cell, '#808080', [true]), cell)));
            } else {
                // CANVAS.arrayRect.map(cell => (this.drawElement(cell, '#808080', [true]), cell));
                // CANVAS.arrayRect.map(cell => (this.drawElement(cell, '#ff0000', [cell.name().includes((<HTMLInputElement>e.target).value)]), cell));
                CANVAS.arrayRect.map((gr: Konva.Group) => {
                    gr.children.each(
                        (cell: Konva.Rect) => {
                            this.drawElement(cell, '#808080', [true]), cell;
                            this.drawElement(cell, '#ff0000', [cell.id().includes((<HTMLInputElement>e.target).value)]), cell;
                        }
                    )
                });
            }
            CANVAS.layer.draw();
        });
        document.querySelector('#search_another_markets').addEventListener('click', e => {
            e.stopPropagation();
            e.preventDefault();
            const TYPE_ID_ARRAY: number[] = [];
            const MANAGER_ARRAY: number[] = [];
            document.querySelectorAll('#list_checkbox input')
                .forEach((val: HTMLInputElement) => val.checked ? TYPE_ID_ARRAY.push(+val.value) : null);
            document.querySelectorAll('#list_managers input')
                .forEach((val: HTMLInputElement) => val.checked ? MANAGER_ARRAY.push(+val.value) : null);

            this.http
                .searchAllCellsLike(
                    (<HTMLInputElement>document.querySelector('#search_nomenclat')).value, // ax-id
                    TYPE_ID_ARRAY,                                                         // type-array
                    +(<HTMLInputElement>document.querySelector('#balance')).value,         //
                    MANAGER_ARRAY,
                    (<HTMLInputElement>document.querySelector('#search_nomenclat_cells')).value,
                    +(<HTMLSelectElement>document.querySelector('#search_nomenclat_producers')).dataset.id,
                    +(<HTMLSelectElement>document.querySelector('#search_nomenclat_collection')).dataset.id,
                )
                .subscribe((arrayMarkets: ShopsSearchItems[]) => {
                    arrayMarkets ? this.renderCellAndShopDataList(arrayMarkets, 'cells_markets_cells', CANVAS) : {};
                });
        });
    }
    /**
     * @param  {Array<Konva.Rect|Konva.Group>} element
     * @param  {Array<Boolean>} conditions array conditions 
     * @returns void
     */
    drawElement(element: Konva.Rect | Konva.Group, color: string, conditions: Array<Boolean>): void {
        if (!conditions.includes(false) && element instanceof Konva.Rect) {
            element.fill(color);
        };
        if (!conditions.includes(false) && element instanceof Konva.Group) {
            element.children.each((cellChild: Konva.Rect) => {
                cellChild.fill(color);
            });
        };
    }
    /**
     * @param  {CellsSearchItems[]} array -- array cells with id-shop and div_id-shop 
     * @param  {number} id  -- id datalist in html attribute. Datalist write array.
     * @returns void  n021396
     */
    renderCellAndShopDataList(array: ShopsSearchItems[], id: string, canvas: WorkField): void {
        
        document.querySelector(`#${id}`).innerHTML = '';
        sessionStorage.setItem('cells-seach-items', JSON.stringify(array));
        array.map(cell => {
            const div = document.createElement('div');
            div.textContent = `${cell.NAME} ==> ${cell.WMS_AX_ID_STR}`;
            div.setAttribute('data-div_id', '' + cell.DIVISION_ID);
            div.setAttribute('data-wms_ax_id_str', cell.WMS_AX_ID_STR);
            div.id = `search_select_${cell.DIVISION_ID}-${cell.WMS_AX_ID_STR}`;
            div.setAttribute('data-value', `search_select_${cell.DIVISION_ID}-${cell.WMS_AX_ID_STR}`);
            this.addEvent2SelectSearchItems(div, canvas);    
            document.querySelector(`#${id}`).appendChild(div);
        });
        
    }
    addMenuEvents(CANVAS: WorkField): void { // # говнокод добавление event в render-class
        document.querySelector('#active_level').textContent =
            sessionStorage.getItem('AX_ID') ??
            (<HTMLLIElement>document.querySelector('.menu-item')).dataset.wms_ax_id_str;
        document.querySelectorAll('.menu-item').forEach(itemNodeElement =>
            itemNodeElement.addEventListener('click', itemTargetElement => {
                document.querySelectorAll('.menu-item').forEach((menuItem: HTMLLIElement) => {
                    (<HTMLSpanElement>menuItem.querySelector('.text_market')).style.color = '#000';
                });
                document.querySelector('#active_level').textContent = (<HTMLLIElement>itemNodeElement).dataset.wms_ax_id_str;
                (<HTMLSpanElement>itemNodeElement.querySelector('.text_market')).style.color = '#f44336';
                sessionStorage.setItem('AX_ID', ((<HTMLLIElement>itemNodeElement).dataset.wms_ax_id_str));
                sessionStorage.setItem('DIV_ID', ((<HTMLLIElement>itemNodeElement).dataset.div_id));
                closeAllPopup();
                this.http.getPlanPathImage(sessionStorage.getItem('AX_ID')).subscribe(data => {
                    if (!data || !data.length) return;
                    if (data.length === 1) {
                        let imageSrc = `/${data[0].URL}`;
                        this.market = {
                            container: 'container',
                            width: 1000,
                            height: 1000,
                            imageSrc: imageSrc
                        }
                        this.clearPage(CANVAS);
                        closeAllPopup();
                        return;
                    }
                    const PLAN = data.reduce((accumulate: any, current: any) => { // найти план этажа для загрузки на страницу
                        if (!accumulate) {
                            return current
                        } else {
                            return new Date(accumulate.DATE) > new Date(current.DATE) ? accumulate : current
                        }
                    })
                    let imageSrc = `/${PLAN?.URL}`;
                    this.market = {
                        container: 'container',
                        width: 1000,
                        height: 1000,
                        imageSrc: imageSrc
                    };
                    closeAllPopup();
                    this.clearPage(CANVAS);
                }, null, () => closeAllPopup());
            })
        );
    }
    reqJoin(arrayFunctionPrerender: Array<Observable<Cell[]>>, CANVAS: WorkField) {
        const forkStream$ = forkJoin(arrayFunctionPrerender).subscribe(async (data) => {              // render page
            await this.renderCanvasCells(<Cell[]>data[0], CANVAS);                  // render canvas cells
            this.renderListSelect(data[1], CANVAS.newOption)                  // # говнокод необходим интерфейс пробрасывается any // render datalist 
            this.arrayCells = <Cell[]>data[1];                                // add array NOT canvas cells in const
            // forkStream$.unsubscribe();
        });
    }

    buildArr(): Array<Observable<any>> {
        return [
            this.http.getArrayCellsCanvas(
                sessionStorage.getItem('DIV_ID') ??
                (<HTMLOrSVGElement><Object>document.querySelector('.menu-item'))?.dataset?.div_id,
                sessionStorage.getItem('AX_ID') ??
                (<HTMLOrSVGElement><Object>document.querySelector('.menu-item'))?.dataset?.wms_ax_id_str),
            this.http.getArrayCells(
                sessionStorage.getItem('DIV_ID') ??
                (<HTMLOrSVGElement><Object>document.querySelector('.menu-item'))?.dataset?.div_id,
                sessionStorage.getItem('AX_ID') ??
                (<HTMLOrSVGElement><Object>document.querySelector('.menu-item'))?.dataset?.wms_ax_id_str
            ),
            this.http.getTypesMaterials(),
            this.http.getProducerItem(),
            this.http.getManagers()
            // this.http.getCollectionItem()
        ];

    }
    clearPage(CANVAS: WorkField): void {
        CANVAS.renderCanvas(this.market);
        CANVAS.arrayRect = [];
        CANVAS.transformElements.nodes([]);
        CANVAS.optionClear();                      // # говнокод исправить (не критично)
        this.reqJoin(this.buildArr(), CANVAS);
    }
    clearPageSubscriber(CANVAS: WorkField): Observable<any> { // # говнокод исправить (не критично)
        CANVAS.renderCanvas(this.market);
        CANVAS.arrayRect = [];
        CANVAS.transformElements.nodes([]);
        CANVAS.optionClear();
        // this.reqJoin(this.buildArr(), CANVAS);
        return forkJoin(this.buildArr())
    }
    /**
     * @param  {Producer[]} arrayProducers
     * @returns void
     * Render list producer in data-list filter component.
     */
    renderDataListProducer(arrayProducers: Producer[]): void {
        arrayProducers?.sort((a: Producer, b: Producer) =>
            a.NAME < b.NAME ? -1 : 1
        ).map(producer => {
            const div = document.createElement('div');
            div.textContent = producer.NAME;
            div.id = '' + producer.NAME;
            div.setAttribute('data-id', '' + producer.ID);
            div.setAttribute('value', '' + producer.ID);
            div.classList.add('list_item');
            div.addEventListener('click', e => {
                (<HTMLInputElement>document.querySelector('#search_nomenclat_producers')).value = '' + producer.NAME;
                (<HTMLInputElement>document.querySelector('#search_nomenclat_producers')).dataset.id = '' + producer.ID;
                this.http
                    .getCollectionItem(+(<HTMLDivElement>e.target).dataset.id)
                    .subscribe(this.renderCollectionSelect);
            });
            document.querySelector('#lsit_producers').appendChild(div);
        });
    }

    renderCollectionSelect(arrayCollecton: Collection[]): void {
        document.querySelector('#lsit_collections').innerHTML = '';                                 // clear field input
        (<HTMLInputElement>document.querySelector('#search_nomenclat_collection')).value = '';      // clear value input
        (<HTMLInputElement>document.querySelector('#search_nomenclat_collection')).dataset.id = ''; // clear id input
        const OPT = document.createElement('div');
        document.querySelector('#lsit_collections').appendChild(OPT);
        arrayCollecton?.sort((a: Collection, b: Collection) => a.NAME < b.NAME ? -1 : 1)            // sort array collection
            .map(collection => {                                                                    // loop create element 
                const div = document.createElement('div');
                div.textContent = collection.NAME || collection.AX_ID;
                div.setAttribute('data-id', '' + collection.ID);
                div.setAttribute('id', '' + collection.ID);
                div.classList.add('list_item');
                div.addEventListener('click', e => {
                    (<HTMLInputElement>document
                        .querySelector('#search_nomenclat_collection'))
                        .value = '' + collection.NAME;
                    (<HTMLInputElement>document
                        .querySelector('#search_nomenclat_collection'))
                        .dataset.id = '' + collection.ID;
                });
                document.querySelector('#lsit_collections').appendChild(div);
            });
    }
    renderManagers(arrayManagers: Manager[]): void {
        const LIST_CHECKBOX = document.querySelector('#list_managers');
        arrayManagers.map(manager => {
            const DIV = document.createElement('div');
            const LABLEL = document.createElement('label');
            const INPUT = document.createElement('input');
            DIV.setAttribute('class', 'item_manager')
            INPUT.setAttribute('type', 'checkbox');
            INPUT.checked = true;
            LABLEL.textContent = manager.NAME;
            LABLEL.setAttribute('for', 'manager_' + manager.ID);
            INPUT.setAttribute('value', '' + manager.ID);
            INPUT.setAttribute('name', '' + manager.ID);
            INPUT.setAttribute('id', 'manager_' + manager.ID);
            DIV.appendChild(INPUT);
            DIV.appendChild(LABLEL);
            LIST_CHECKBOX.appendChild(DIV);
        });
    }

    renderSearchTypeCheckbox(arrayTypeItems: TypeItems[]) {
        const LIST_CHECKBOX = document.querySelector('#list_checkbox');
        arrayTypeItems.map(typeItem => {
            const DIV = document.createElement('div');
            const LABLEL = document.createElement('label');
            const INPUT = document.createElement('input');
            DIV.setAttribute('class', 'item_type');
            INPUT.setAttribute('type', 'checkbox');
            INPUT.checked = true;
            LABLEL.textContent = typeItem.NAME;
            LABLEL.setAttribute('for', 'item_type_' + typeItem.ID);
            INPUT.setAttribute('value', '' + typeItem.ID);
            INPUT.setAttribute('name', typeItem.AX_ID);
            INPUT.setAttribute('id', 'item_type_' + typeItem.ID);
            DIV.appendChild(INPUT);
            DIV.appendChild(LABLEL);
            LIST_CHECKBOX.appendChild(DIV);
        });
    }

    addEventInputFilter(): void {
        document.querySelector('#search_nomenclat_producers').addEventListener('keyup', e => {
            const FIND_STR = (<HTMLInputElement>e.target).value.toLocaleLowerCase();
            document.querySelector('#lsit_producers').querySelectorAll('.list_item').forEach(producer =>
                (<HTMLOptionElement>producer).textContent.toLocaleLowerCase().includes(FIND_STR) ?
                    (<HTMLOptionElement>producer).style.display = 'block' :
                    (<HTMLOptionElement>producer).style.display = 'none'
            );
        });
        document.querySelector('#search_nomenclat_collection').addEventListener('keyup', e => {
            const FIND_STR = (<HTMLInputElement>e.target).value.toLocaleLowerCase();
            document.querySelector('#lsit_collections').querySelectorAll('.list_item').forEach(collection =>
                (<HTMLOptionElement>collection).textContent.toLocaleLowerCase().includes(FIND_STR) ?
                    (<HTMLOptionElement>collection).style.display = 'block' :
                    (<HTMLOptionElement>collection).style.display = 'none'
            );
        });
        document.querySelector('#search_nomenclat_cells_work').addEventListener('keyup', e => {
            const FIND_STR = (<HTMLInputElement>e.target).value.toLocaleLowerCase();
            document.querySelector('#lsit_cells').querySelectorAll('.list_item').forEach(collection =>
                (<HTMLOptionElement>collection).textContent.toLocaleLowerCase().includes(FIND_STR) ?
                    (<HTMLOptionElement>collection).style.display = 'block' :
                    (<HTMLOptionElement>collection).style.display = 'none'
            );
        });
    }
    createlistImage(levels: Menu[]) {
        const LIST_CONTAINER = document.querySelector('#list__levels');
        levels.sort(this.sortMenu).map(level => {
            const ITEM = document.createElement('div');
            ITEM.textContent = level.WMS_AX_ID_STR;
            // <div class="option" onclick="selectOption(event);">Option 1</div>
            ITEM.classList.add('option');
            ITEM.addEventListener('click', selectOption);
            LIST_CONTAINER.appendChild(ITEM);
        })
    }

    sortMenu(a: Menu, b: Menu) {
        if (a.WMS_AX_ID_STR < b.WMS_AX_ID_STR) return - 1;
        if (a.WMS_AX_ID_STR > b.WMS_AX_ID_STR) return 1;
        return 0;
    }
    buildItem(objectList: Array<ItemProduct>, wrapper: HTMLLIElement, axId: string): void {
        document.querySelector('#cell_info_name').textContent = axId;
        wrapper.innerHTML = '';
        if (objectList) {
            objectList.forEach((el: ItemProduct) => {
                let item = document.createElement('div');
                item.classList.add('data_item');

                let imageContainer = document.createElement('div');
                imageContainer.classList.add('data_item_image');

                let image = document.createElement('img');
                image.setAttribute('src', IMAGE_HOSTNAME + el.PATH);

                let infoContainer = document.createElement('div');
                infoContainer.classList.add('data_item_info');

                let axid = document.createElement('div');
                axid.classList.add('data_item_info_axid');
                axid.textContent = el.ARTICLE;

                let name = document.createElement('div');
                name.classList.add('data_item_info_name');
                name.textContent = el.NAME;

                let qty = document.createElement('div');
                qty.classList.add('data_item_info_qty');
                qty.textContent = `Остаток: ${el.TOTAL}`;

                imageContainer.appendChild(image);
                item.appendChild(imageContainer);
                infoContainer.appendChild(axid);
                infoContainer.appendChild(name);
                infoContainer.appendChild(qty);
                item.appendChild(infoContainer);
                wrapper.appendChild(item);
            });
        } else {
            let emptyList = document.createElement('div');
            emptyList.classList.add('data_no_items');
            emptyList.textContent = 'Нет товаров';
            wrapper.appendChild(emptyList);
        }
    }

    autorizedUIevents(selector?: string) {
        (<HTMLButtonElement>document
            .querySelector(selector || 'button#enteries'))
            .addEventListener('click', (element: Event) => {
                element.preventDefault();   // останавливаем 
                element.stopPropagation();  // стандартное поведение тегов
                const ARRAY_INPUT = {};
                (<HTMLButtonElement>element.composedPath()[0]).closest('form').querySelectorAll('input').forEach((input: HTMLInputElement) =>  // проходим по форме, считывем все input
                    Object.defineProperty(ARRAY_INPUT, input.getAttribute('name'), {     // добавляем значение свойства ключ-значение
                        value: input.value,
                        enumerable: true
                    })
                );
                this.http.autorized(ARRAY_INPUT)
                    .pipe(finalize(() => (this.loadData(), window.location.reload())))
                    .subscribe(
                        autorizeData => (
                            localStorage.setItem('token', autorizeData.token),
                            localStorage.setItem('ug', JSON.stringify(autorizeData.user.groups))
                        )
                        , error => console.error(error));

            });
        (<HTMLButtonElement>document.querySelector('button#exit')).addEventListener('click', (button: Event) => {
            this.http.removeNotValidAutorizedData();
            window.location.reload();
        })
        // ent.preventDefault();   // останавливаем 
        //     element.stopPropagation();  // стандартное поведение тегов
        //     this.http.autorized(        // отправляем форму на бек. (HTMLFormElement)
        //         (<HTMLButtonElement>element.composedPath()[0])  //получаем вложенность тегов, выбираем 0-вой(button) 
        //         .closest('form'))       // от кнопки "идем" вверх и ищем тег form
    }
}













