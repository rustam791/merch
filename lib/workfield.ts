/**
 * PROJECT MERCH
 * Builder canvas.
 * Control state rect and group element.
 * 
 * Version 1.3
 */

import Konva from "konva";
import { Calculate, Point } from "../interface/calculate";

import { Cell, CellStartRender, GroupCell } from '../interface/cell';
import { CallBackObj } from '../interface/prototype';
import { Collection } from 'konva/types/Util';
import { interval, Observable, of } from "rxjs";
import { observeOn } from "rxjs/operators";
import { Group } from "konva/types/Group";
import { Canvas } from "konva/types/Canvas";
export const SCALE = 1.25;
export const ID_ONCE = 1;
export const ID_GROUP = 21;
export const WIDTH_GROUP_CELL = 50;
export const HEIGTH_GROUP_CELL = 5;
export const WIDTH_GROUP = 120;
export const HEIGTH_GROUP = 90;
export const ADMIN_GROUP = 'dj_merch_a'
// export const TYPE_OBJECT_FACTORY = {
//     'кулла': 
// }


export class WorkField {

    arrayRect: Array<Konva.Group>;
    arrayRenderedElement: Array<Konva.Group>;
    transformElements: Konva.Transformer = new Konva.Transformer();
    layer: Konva.Layer;
    width: number;
    height: number;
    stage: Konva.Stage;
    imageKonva: Konva.Image;
    image: HTMLImageElement;



    TYPE_OBJECT_FACTORY = {
        'кулла': this.buildKulla,
        'стенд книжка': this.buildStandBook,
        'стенд напольный': this.buildKulla,
        'стенд': this.buildKulla,
        'default': this.buildDefaultGroup
    }



    constructor(config: any, autorized?: string) {          // # говнокод
        this.renderCanvas(config);
        this.arrayRect = [];
        this.arrayRenderedElement = [];
        this.transformElements.zIndex(10);
    }


    buildKulla(cellGr: GroupCell, arrayCell: Cell[], callbackObject?: CallBackObj): Konva.Group {
        const HEIGTH_DB_CELL = ((cellGr.HEIGHT ?? 100) / arrayCell.length) / 2;
        const textIdCells = new Konva.Text({
            text: cellGr.AX_ID,
            fontFamily: 'Calibri',
            fontSize: 12 / SCALE,
            padding: 5 / SCALE,
            textFill: 'white',
            fill: 'black',
            alpha: 0.75,
            visible: false,
        });

        // this.layer.add(textIdCells);
        // this.layer.draw();

        const option = {
            id: '' + cellGr.WMS_ID,
            x: cellGr.M_X ?? 100,
            y: cellGr.M_Y ?? 100,
            draggable: this.checkUserAdmin(),
            stroke: 'black',
            strokeWidth: 4,
            fill: 'red',
            name: cellGr.AX_ID,
            check: true,
            WMS_ID: cellGr.WMS_ID
        }
        if (cellGr.WIDTH) {
            Object.defineProperty(option, 'width', {
                value: cellGr.WIDTH,
                configurable: true,
                enumerable: true
            });
            option.check = false;
        }
        if (cellGr.HEIGHT) {
            Object.defineProperty(option, 'height', {
                value: cellGr.HEIGHT,
                configurable: true,
                enumerable: true
            });
        }
        if (cellGr.ANGLE) {
            Object.defineProperty(option, 'rotation', {
                value: cellGr.ANGLE,
                configurable: true,
                enumerable: true
            });
        }

        let gr = new Konva.Group(option);
        this.addNameCellToField(cellGr);

        arrayCell.sort((a, b) => {
            if (a.AX_ID < b.AX_ID) {
                return -1;
            }
            if (a.AX_ID > b.AX_ID) {
                return 1;
            }
            return 0;
        }).map((cell: Cell, index: number) => {
            const REC_LEFT = new Konva.Rect({
                x: 0,
                y: index * HEIGTH_DB_CELL * 2,
                height: isNaN(HEIGTH_DB_CELL) ? 10 : HEIGTH_DB_CELL,
                width: cellGr.WIDTH ?? 100,
                fill: 'grey',
                id: cell.AX_ID,
                WMS_ID: cell.WMS_ID
            });
            REC_LEFT.on('click', (e: any) => {
                this.arrayRect.map((gr: Konva.Group) => gr.children.toArray().map((rect: Konva.Rect) => { rect.attrs.fill = 'grey' }));
                REC_LEFT.fill('red');
                this.layer.draw();
            });
            gr.add(this.builGroupRect(cell, REC_LEFT));
        });

        gr.height(HEIGTH_DB_CELL * arrayCell.length * 2);
        gr.width(gr.children[gr.children.length - 1].x() + gr.children[gr.children.length - 1].width());



        gr.on(callbackObject.event, callbackObject.func);


        // if (gr.children.length === 1) {
        //     gr.width(gr.children[0].x() + gr.children[0].width());
        //     gr.height(gr.children[0].height());
        // } else {
        //     gr.width(gr.children[1].x() + gr.children[1].width());
        //     gr.height(gr.children[gr.children.length - 1].y());
        // }

        textIdCells.position({
            x: gr.x(),
            y: gr.y() + gr.height() + HEIGTH_GROUP_CELL
        });

        gr = this.addEventGroupKulla(gr); // #refact
        this.arrayRect.push(gr);
        return gr;
    }

    buildStandBook(cellGr: GroupCell, arrayCell: Cell[], callbackObject?: CallBackObj): Konva.Group {
        const MARGIN_WIDTH = cellGr.WIDTH - WIDTH_GROUP_CELL * (cellGr.WIDTH / WIDTH_GROUP) * 2;
        const WIDTH_DB_CELL = (cellGr.WIDTH - (cellGr.WIDTH - WIDTH_GROUP_CELL * (cellGr.WIDTH / WIDTH_GROUP) * 2)) / 2;

        let items = Math.ceil(arrayCell.length / 2);
        const MARGIN_HEIGTH = (cellGr.HEIGHT - HEIGTH_GROUP_CELL * (cellGr.HEIGHT / HEIGTH_GROUP) * items) / items;
        // console.log(items);

        // const MARGIN_HEIGTH = (cellGr.HEIGHT - HEIGTH_GROUP_CELL * (cellGr.HEIGHT / HEIGTH_GROUP)) / items;
        // const HEIGTH_DB_CELL = (cellGr.HEIGHT - (cellGr.HEIGHT - HEIGTH_GROUP_CELL * (cellGr.HEIGHT / HEIGTH_GROUP) * items)) / items;
        const HEIGTH_DB_CELL = cellGr.HEIGHT / (arrayCell.length - 1)

        const textIdCells = new Konva.Text({
            text: cellGr.AX_ID,
            fontFamily: 'Calibri',
            fontSize: 12 / SCALE,
            padding: 5 / SCALE,
            textFill: 'white',
            fill: 'black',
            alpha: 0.75,
            visible: false
        });


        // this.layer.add(textIdCells);
        // this.layer.draw();

        const option = {
            id: '' + cellGr.WMS_ID,
            x: cellGr.M_X ?? 100,
            y: cellGr.M_Y ?? 100,
            draggable: this.checkUserAdmin(),
            stroke: 'black',
            strokeWidth: 4,
            fill: 'red',
            name: cellGr.AX_ID,
            check: true,
            WMS_ID: cellGr.WMS_ID
        }
        if (cellGr.WIDTH) {
            Object.defineProperty(option, 'width', {
                value: cellGr.WIDTH,
                configurable: true,
                enumerable: true
            });
            option.check = false;
        }
        if (cellGr.HEIGHT) {
            Object.defineProperty(option, 'height', {
                value: cellGr.HEIGHT,
                configurable: true,
                enumerable: true
            });
        }
        if (cellGr.ANGLE) {
            Object.defineProperty(option, 'rotation', {
                value: cellGr.ANGLE,
                configurable: true,
                enumerable: true
            });
        }

        let gr = new Konva.Group(option);
        this.addNameCellToField(cellGr);

        gr.on('transformend', (e: any) => {
            try {   // установить параметры ax_id ячейки 
                const TEXT: Konva.Text = this.layer.findOne(`#${cellGr ? cellGr.WMS_ID : gr.id()}`);
                TEXT.x(gr.x() + gr.height() * Math.sin((gr.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                TEXT.y(gr.y() - gr.height() * Math.cos((gr.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                TEXT.rotation(gr.rotation());
                // const s: number = elem.scaleX()
                // TEXT.fontSize(TEXT.fontSize() * gr.scaleX());
            } catch { }
            this.layer.draw()
            this.detectCollision(this.calculate(), this.layer, this.objectPoint({
                height: gr.height(),
                width: gr.width(),
                x: gr.x(),
                y: gr.y(),
                rotate: gr.rotation(),
                id: gr.id(),
                name: gr.name()
            }));
            textIdCells.hide();
            this.stage.draw();
        });
        gr.on('dragend', (e: any) => { // dragged
            let scaleLayer = 0;
            switch (true) {
                case +this.layer.scaleY().toFixed() <= 3:
                    scaleLayer = 1;
                    break;
                default:
                    scaleLayer = 2;
                    break;
            }

            gr.x(+(+gr.x().toFixed(scaleLayer) / 5).toFixed(scaleLayer - 1) * 5);
            gr.y(+(+gr.y().toFixed(scaleLayer) / 5).toFixed(scaleLayer - 1) * 5);

            this.detectCollision(this.calculate(), this.layer, this.objectPoint({
                height: gr.height(),
                width: gr.width(),
                x: gr.x(),
                y: gr.y(),
                rotate: gr.rotation(),
                id: gr.id(),
                name: gr.name()
            }));
            try {
                const TEXT: Konva.Text = this.layer.findOne(`#${cellGr ? cellGr.WMS_ID : gr.id()}`);
                TEXT.x(gr.x() + gr.height() * Math.sin((gr.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                TEXT.y(gr.y() - gr.height() * Math.cos((gr.rotation() / (180 / Math.PI)) + 1.5708 * 2));
            } catch (err) {
                console.log(gr);
                console.log(cellGr);
                console.log(err);
            }

            textIdCells.hide();
            this.stage.draw();
        });



        gr.on(callbackObject.event, callbackObject.func);
        arrayCell.sort((a, b) => {
            if (a.AX_ID < b.AX_ID) {
                return -1;
            }
            if (a.AX_ID > b.AX_ID) {
                return 1;
            }
            return 0;
        }).map((cell: Cell, index: number) => {
            if (index % 2) {
                const REC_RIGHT = new Konva.Rect({
                    x: isNaN(WIDTH_DB_CELL + MARGIN_WIDTH) ? 0 + 70 : WIDTH_DB_CELL + MARGIN_WIDTH,       // из-за отступов нужно рассчитывать так
                    //!!! ОТСТУПЫ ПО ВЕРТИКАЛИ
                    y: isNaN((index - 1) * (HEIGTH_DB_CELL)) ? 0 + (index - 1) * 5 : (index - 1) * (HEIGTH_DB_CELL), //+ (HEIGTH_DB_CELL + MARGIN_HEIGTH),    //!!! ОТСТУПЫ ПО ВЕРТИКАЛИ
                    //!!! ОТСТУПЫ ПО ВЕРТИКАЛИ
                    height: isNaN(HEIGTH_DB_CELL) ? 5 : HEIGTH_DB_CELL,
                    width: isNaN(WIDTH_GROUP_CELL * (cellGr.WIDTH / WIDTH_GROUP)) ? 50 : WIDTH_GROUP_CELL * (cellGr.WIDTH / WIDTH_GROUP),
                    fill: 'grey',
                    id: cell.AX_ID,
                    WMS_ID: cell.WMS_ID
                });
                REC_RIGHT.on('click', (e: any) => {
                    // this.arrayRect.map((rect: Konva.Rect) => {rect.attrs.fill = 'grey'});
                    this.arrayRect.map((gr: Konva.Group) => gr.children.toArray().map((rect: Konva.Rect) => { rect.attrs.fill = 'grey' }));
                    REC_RIGHT.fill('red');
                    this.layer.draw();
                });
                gr.add(this.builGroupRect(cell, REC_RIGHT));
            } else {
                const REC_LEFT = new Konva.Rect({
                    x: 0,
                    y: isNaN(index * (HEIGTH_DB_CELL)) ? 0 + index * 5 : index * (HEIGTH_DB_CELL),
                    height: isNaN(HEIGTH_DB_CELL) ? 5 : HEIGTH_DB_CELL,
                    width: isNaN(WIDTH_GROUP_CELL * (cellGr.WIDTH / WIDTH_GROUP)) ? 50 : WIDTH_GROUP_CELL * (cellGr.WIDTH / WIDTH_GROUP),
                    fill: 'grey',
                    id: cell.AX_ID,
                    WMS_ID: cell.WMS_ID
                });
                REC_LEFT.on('click', (e: any) => {
                    this.arrayRect.map((gr: Konva.Group) => gr.children.toArray().map((rect: Konva.Rect) => { rect.attrs.fill = 'grey' }));
                    REC_LEFT.fill('red');
                    this.layer.draw();
                });
                gr.add(this.builGroupRect(cell, REC_LEFT));
            }
        });

        if (gr.children.length % 2) {
            if (gr.children.length === 1) {
                gr.width(gr.children[0].x() + gr.children[0].width());
                gr.height(gr.children[0].height());
            } else {
                gr.width(gr.children[1].x() + gr.children[1].width());
                gr.height(gr.children[gr.children.length - 1].y());
            }
        } else {
            gr.width(gr.children[gr.children.length - 1].x() + gr.children[gr.children.length - 1].width());
            gr.height(gr.children[gr.children.length - 1].y() + gr.children[gr.children.length - 1].height());
        };

        gr = this.addEventGroup(gr); // #refact
        this.arrayRect.push(gr);
        return gr;

    }

    buildDefaultGroup(cellGr: GroupCell, arrayCell: Cell[], callbackObject?: CallBackObj): Konva.Group {

        const textIdCells = new Konva.Text({
            text: cellGr.AX_ID,
            fontFamily: 'Calibri',
            fontSize: 12 / SCALE,
            padding: 5 / SCALE,
            textFill: 'white',
            fill: 'black',
            alpha: 0.75,
            visible: false,
        });


        // this.layer.add(textIdCells);
        // this.layer.draw();

        const option = {
            id: '' + cellGr.WMS_ID,
            x: cellGr.M_X ?? 100,
            y: cellGr.M_Y ?? 100,
            draggable: this.checkUserAdmin(),
            stroke: 'black',
            strokeWidth: 4,
            fill: 'red',
            name: cellGr.AX_ID,
            check: true,
            WMS_ID: cellGr.WMS_ID
        }
        if (cellGr.WIDTH) {
            Object.defineProperty(option, 'width', {
                value: cellGr.WIDTH,
                configurable: true,
                enumerable: true
            });
            option.check = false;
        }
        if (cellGr.HEIGHT) {
            Object.defineProperty(option, 'height', {
                value: cellGr.HEIGHT,
                configurable: true,
                enumerable: true
            });
        }
        if (cellGr.ANGLE) {
            Object.defineProperty(option, 'rotation', {
                value: cellGr.ANGLE,
                configurable: true,
                enumerable: true
            });
        }

        let gr = new Konva.Group(option);
        this.addNameCellToField(cellGr);

        arrayCell.map((cell: Cell, index: number) => {
            const REC_LEFT = new Konva.Rect({
                x: 0,
                y: 0,
                height: cellGr.HEIGHT ?? 100,
                width: cellGr.WIDTH ?? 100,
                fill: 'grey',
                id: cell.AX_ID,
                WMS_ID: cell.WMS_ID
            });
            REC_LEFT.on('click', (e: any) => {
                this.arrayRect.map((gr: Konva.Group) => gr.children.toArray().map((rect: Konva.Rect) => { rect.attrs.fill = 'grey' }));
                REC_LEFT.fill('red');
                this.layer.draw();
            });
            gr.add(this.builGroupRect(cell, REC_LEFT));
        });

        gr.height(cellGr.HEIGHT ?? 100);
        gr.width(cellGr.WIDTH ?? 100);



        gr.on(callbackObject.event, callbackObject.func);


        // if (gr.children.length === 1) {
        //     gr.width(gr.children[0].x() + gr.children[0].width());
        //     gr.height(gr.children[0].height());
        // } else {
        //     gr.width(gr.children[1].x() + gr.children[1].width());
        //     gr.height(gr.children[gr.children.length - 1].y());
        // }

        textIdCells.position({
            x: gr.x(),
            y: gr.y() + HEIGTH_GROUP_CELL
        });

        gr = this.addEventDefaultGroup(gr); // #refact
        this.arrayRect.push(gr);
        return gr;
    }

    addEventDefaultGroup(group: Konva.Group): Konva.Group {
        group.on('dragend', (e: any) => { // dragged
            let scaleLayer = 0;
            switch (true) {
                case +this.layer.scaleY().toFixed() <= 3:
                    scaleLayer = 1;
                    break;
                default:
                    scaleLayer = 2;
                    break;
            }

            group.x(+(+group.x().toFixed(scaleLayer) / 5).toFixed(scaleLayer - 1) * 5);
            group.y(+(+group.y().toFixed(scaleLayer) / 5).toFixed(scaleLayer - 1) * 5);

            this.detectCollision(this.calculate(), this.layer, this.objectPoint({
                height: group.height(),
                width: group.width(),
                x: group.x(),
                y: group.y(),
                rotate: group.rotation(),
                id: group.id(),
                name: group.name()
            }));
            try {
                const TEXT: Konva.Text = this.layer.findOne(`#${group.id()}`);
                TEXT.x(group.x() + group.height() * Math.sin((group.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                TEXT.y(group.y() - group.height() * Math.cos((group.rotation() / (180 / Math.PI)) + 1.5708 * 2));
            } catch (err) {
                console.log(err);
            }
            textIdCells.hide();
            this.stage.draw();
        });

        group.on('transformend', (e: any) => {
            (<Collection<Konva.Rect>>group.children).each(chiledren => {
                chiledren.y(chiledren.y() * group.scaleY());
                chiledren.x(chiledren.x() * group.scaleX());
                chiledren.width(chiledren.width() * group.scaleX());
                chiledren.height(chiledren.height() * group.scaleY());
            });

            group.scale({ x: 1, y: 1 });

            group.width(group.children[0].x() + group.children[0].width())
            group.height(group.children[0].height() * group.children.length)   // set width, heigth group


            try {   // установить параметры ax_id ячейки 
                const TEXT: Konva.Text = this.layer.findOne(`#${group.id()}`);
                TEXT.x(group.x() + group.height() * Math.sin((group.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                TEXT.y(group.y() - group.height() * Math.cos((group.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                TEXT.rotation(group.rotation());
            } catch { }
            this.detectCollision(this.calculate(), this.layer, this.objectPoint({
                height: group.height(),
                width: group.width(),
                x: group.x(),
                y: group.y(),
                rotate: group.rotation(),
                id: group.id(),
                name: group.name()
            }));
            textIdCells.position({
                x: group.x(),
                y: group.y() + group.height() + HEIGTH_GROUP_CELL
            });

            this.layer.draw();
            this.stage.draw();
            // const textIdCells = new Konva.Text({
            //     text: '',
            //     fontFamily: 'Calibri',
            //     fontSize: 12 / SCALE,
            //     padding: 5 / SCALE,
            //     textFill: 'white',
            //     fill: 'black',
            //     alpha: 0.75,
            //     visible: false,
            // });
        });
        if (this.checkUserAdmin()) {
            group.on('dblclick', () => {
                this.arrayRect.splice(this.arrayRect.indexOf(group), 1);
                group.fire('mouseout');
                this.newOption(group);
                group.remove();
                this.transformElements.nodes([]);
                this.stage.container().style.cursor = 'default';
            });
            group.on('click', () => {
                this.transformElements.nodes([group]);
            });
        }
        group.on('dragend', () => { // dragged
            this.detectCollision(this.calculate(), this.layer, this.objectPoint({
                height: group.height(),
                width: group.width(),
                x: group.x(),
                y: group.y(),
                rotate: group.rotation(),
                id: group.id(),
                name: group.name()
            }));
            this.stage.draw();
        });

        const textIdCells = new Konva.Text({
            text: '',
            fontFamily: 'Calibri',
            fontSize: 12,
            padding: 5,
            textFill: 'white',
            fill: 'black',
            alpha: 0.75,
            visible: false,
        });
        return group;
    }




    renderCanvas(config: any): void {
        this.stage = new Konva.Stage(config);
        this.layer = new Konva.Layer();
        this.imageKonva = new Konva.Image();

        this.image = new Image();
        this.image.src = config.imageSrc;
        this.image.onload = e => {
            this.imageKonva = new Konva.Image({
                image: this.image,
                x: this.stage.x(),
                y: this.stage.y(),
                scale: {
                    x: this.stage.width() / this.image.width,
                    y: this.stage.width() / this.image.width
                }
            });
            this.addEventZoom();
            this.layer.add(this.imageKonva);
            this.checkUserAdmin() ? this.layer.add(this.transformElements) : {};
            this.stage.add(this.layer);
        }

    }

    /**
     * @returns void
     */
    addEventZoom(): void {
        let dragged: boolean = false;
        let x: number = 0;
        let y: number = 0;
        /**
         * @param  {} 'mousedown' click in canvas
         * start event navigate canvas
         */
        this.stage.on('mousedown', e => {
            if (e.target instanceof Konva.Image) {
                x = e.evt.clientX - this.layer.x();
                y = e.evt.clientY - this.layer.y();
                dragged = true;
            }
        });

        /**
         * @param  {} 'mouseup'
         * navigate convas if click
         */
        this.stage.on('mousemove', e => {
            if (dragged) {                             // e.target instanceof Konva.Image &&
                this.layer.x(e.evt.clientX - x);
                this.layer.y(e.evt.clientY - y);
                this.stage.draw();
            }
        });

        /**
         * @param  {} 'mouseup'
         */
        this.stage.on('mouseup mouseleave mouseout', e => { // mouseleave ? 
            if (e.target instanceof Konva.Image) {
                dragged = false;
            }
        });

        this.stage.on('wheel', e => {
            // if (e.target instanceof Konva.Image) {
            e.evt.stopPropagation();
            e.evt.preventDefault();
            let oldScale = this.layer.scaleX();
            let point = this.stage.getPointerPosition();
            let pointTo = {
                x: (point.x - this.layer.x()) / oldScale,
                y: (point.y - this.layer.y()) / oldScale
            };
            let newScale = e.evt.deltaY < 0 ? oldScale * SCALE : oldScale / SCALE;
            if (newScale < 1) return;
            this.layer.scale({
                x: newScale,
                y: newScale
            });
            this.layer.position({
                x: point.x - pointTo.x * newScale,
                y: point.y - pointTo.y * newScale
            })
            this.stage.draw();
            // }
        });

        this.imageKonva.on('dblclick', e => {
            this.layer.position({
                x: 0,
                y: 0
            });
            this.layer.scale({
                x: 1,
                y: 1
            });
            this.layer.draw();
        });
    }

    /**
     * @param  {Array<Point>} point
     * @param  {Array<string>} name
     * @returns void
     */
    lineDetect(point: Array<Point>, name: Array<string>): void {
        const detectUa = (  // detect collission Ua
            (point[3].x - point[2].x) * (point[0].y - point[2].y) -
            (point[3].y - point[2].y) * (point[0].x - point[2].x)) /
            ((point[3].y - point[2].y) * (point[1].x - point[0].x) -
                (point[3].x - point[2].x) * (point[1].y - point[0].y));

        const detectUb = (  // detect collission Ub
            (point[1].x - point[0].x) * (point[0].y - point[2].y) -
            (point[1].y - point[0].y) * (point[0].x - point[2].x)) /
            ((point[3].y - point[2].y) * (point[1].x - point[0].x) -
                (point[3].x - point[2].x) * (point[1].y - point[0].y));

        if (detectUa >= 0 && detectUa <= 1 && detectUb >= 0 && detectUb <= 1) {
            // this.arrayRect = this.arrayRect.filter((val: Konva.Rect) => val.id() !== name[0]);
            console.log(this.arrayRect);

            this.arrayRect = this.arrayRect.filter((val: Konva.Group) => {
                if (val.id() !== name[0]) { // ОТРЕФАКТОРИТЬ В ПЕРВУЮ ОЧЕРЕДЬ! ВЫВЕСТИ НА АБСТРАКЦИЮ ВЫШЕ! #говнокод
                    return true;
                } else {
                    this.newOption(val);

                    this.arrayRect.splice(this.arrayRect.indexOf(val), 1);
                    val.fire('mouseout');                                  // dispatche event
                    this.newOption(val);
                    val.remove();
                    return false;
                }
            });
            // delete id/name in 
            this.layer.findOne(`#${this.layer.findOne(`#${name[0]}`).attrs.WMS_ID}`).remove();
            this.layer.findOne(`#${name[0]}`).remove();
            // check user_group
            // # говнокод проверка user - fix вынести выше на уровень управления авторизацией(разделить функционал)
            this.checkUserAdmin() ? this.transformElements.nodes([]) : {};
            this.layer.draw(); // testing!
        }
    };
    /**
     * @param  {any} cell
     * @returns cellisCellStartRender
     */
    instanceOfCellStartRender(cell: any): cell is CellStartRender {
        return cell.INVENT_LOCATION_ID !== undefined;
    }

    /**
     * @param  {Konva.Rect} cell
     * @returns void
     */
    newOption(cell: Konva.Rect): void;
    /**
     * @param  {CellStartRender} cell
     * @returns void
     */
    newOption(cell: Konva.Group): void;
    /**
     * @param  {CellStartRender} cell
     * @returns void
     */
    newOption(cell: CellStartRender): void
    /**
     * @param  {any} cell
     * @returns void
     */
    newOption(cell: any): void {                           // #говнокод - при расширении необходимо будет
        const div = document.createElement('div');
        if (cell.INVENT_LOCATION_ID) {                     // на низком уровне задавать селекторы контейнеров
            div.textContent = `${cell.AX_ID}(${cell.TYPE_CELL})`;                   // отрефакторить, на вход должны идти все параметры и т.д.
            div.classList.add('list_item');
            div.id = '' + cell.AX_ID;
            div.setAttribute('data-id', '' + cell.WMS_ID);
            div.setAttribute('data-group-type', '' + cell.TYPE_NAME);
        } else {
            div.classList.add('list_item');
            div.textContent = cell.name();
            div.id = cell.name();
            div.setAttribute('data-id', cell.attrs.WMS_ID);
        }
        div.addEventListener('click', e => {
            (<HTMLInputElement>document.querySelector('#search_nomenclat_cells_work')).value = '' + cell.AX_ID;
            (<HTMLInputElement>document.querySelector('#search_nomenclat_cells_work')).dataset.id = '' + cell.WMS_ID;
        });
        document.querySelector('#lsit_cells').appendChild(div);
    }

    optionClear(): void {
        document.querySelector('#lsit_cells').innerHTML = '';
    }

    objectPoint(rect: Calculate): Calculate {
        rect.vect = {};
        rect.vect['point1'] = // point origin
        {
            x: rect.x,
            y: rect.y
        };
        rect.vect['point2'] = // right point
        {
            x: rect.x + rect.width * Math.sin((rect.rotate / (180 / Math.PI)) + 1.5708), // 1.5708 - 90(С0)
            y: rect.y - rect.width * Math.cos((rect.rotate / (180 / Math.PI)) + 1.5708)
        };
        rect.vect['point3'] = { // bottom right point
            x: rect.vect.point2.x + rect.height * Math.sin((rect.rotate / (180 / Math.PI)) + 1.5708 * 2),
            y: rect.vect.point2.y - rect.height * Math.cos((rect.rotate / (180 / Math.PI)) + 1.5708 * 2)
        };
        rect.vect['point4'] = // bottom left point
        {
            x: rect.x + rect.height * Math.sin((rect.rotate / (180 / Math.PI)) + 1.5708 * 2),
            y: rect.y - rect.height * Math.cos((rect.rotate / (180 / Math.PI)) + 1.5708 * 2)
        };
        return rect;
    }
    /**
     * @param  {Array<Calculate>} array
     * @param  {Konva.Layer} layer?
     * @param  {Calculate} rect?
     * detect collision points 16 
     */
    detectCollision(array: Array<Calculate>, layer?: Konva.Layer, rect?: Calculate) {
        console.log(array);
        console.log(rect);

        array = array.filter(val => val.id !== rect.id);
        let detectElement = rect.vect;
        array.map((val: any) => {
            try {
                // detected
                this.lineDetect([detectElement.point1, detectElement.point2, val.point1, val.point2], [rect.id, val.id]);
                this.lineDetect([detectElement.point1, detectElement.point2, val.point2, val.point3], [rect.id, val.id]);
                this.lineDetect([detectElement.point1, detectElement.point2, val.point3, val.point4], [rect.id, val.id]);
                this.lineDetect([detectElement.point1, detectElement.point2, val.point4, val.point1], [rect.id, val.id]);

                this.lineDetect([detectElement.point2, detectElement.point3, val.point1, val.point2], [rect.id, val.id]);
                this.lineDetect([detectElement.point2, detectElement.point3, val.point2, val.point3], [rect.id, val.id]);
                this.lineDetect([detectElement.point2, detectElement.point3, val.point3, val.point4], [rect.id, val.id]);
                this.lineDetect([detectElement.point2, detectElement.point3, val.point4, val.point1], [rect.id, val.id]);

                this.lineDetect([detectElement.point3, detectElement.point4, val.point1, val.point2], [rect.id, val.id]);
                this.lineDetect([detectElement.point3, detectElement.point4, val.point2, val.point3], [rect.id, val.id]);
                this.lineDetect([detectElement.point3, detectElement.point4, val.point3, val.point4], [rect.id, val.id]);
                this.lineDetect([detectElement.point3, detectElement.point4, val.point4, val.point1], [rect.id, val.id]);

                this.lineDetect([detectElement.point4, detectElement.point1, val.point1, val.point2], [rect.id, val.id]);
                this.lineDetect([detectElement.point4, detectElement.point1, val.point2, val.point3], [rect.id, val.id]);
                this.lineDetect([detectElement.point4, detectElement.point1, val.point3, val.point4], [rect.id, val.id]);
                this.lineDetect([detectElement.point4, detectElement.point1, val.point4, val.point1], [rect.id, val.id]);
            } catch (e) { }
        });
    };


    builGroupRect(cell: Cell, elem: Konva.Rect): Konva.Rect {
        if (this.checkUserAdmin()) {
            elem.on('click', (e: any) => {
                this.transformElements.nodes([e.target]);
                this.layer.draw();
            });
        }

        elem.on('transformend', (e: any) => {
            elem.rotation(+(elem.rotation() / 15).toFixed() * 15) // grid
            elem.width(elem.scale().x * elem.width())
            elem.height(elem.scale().y * elem.height())
            try {
                const TEXT: Konva.Text = this.layer.findOne(`#${cell.AX_ID}`);
                TEXT.x(elem.x() + elem.height() * Math.sin((elem.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                TEXT.y(elem.y() - elem.height() * Math.cos((elem.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                TEXT.rotation(elem.rotation());
                // TEXT.fontSize(TEXT.fontSize() * elem.scaleX());
            } catch { }
            elem.scale({ x: 1, y: 1 })
            this.layer.draw()
            this.detectCollision(this.calculate(), this.layer, this.objectPoint({
                height: elem.height(),
                width: elem.width(),
                x: elem.x(),
                y: elem.y(),
                rotate: elem.rotation(),
                id: elem.id(),
                name: elem.name()
            }));


            textIdCells.hide();
            this.stage.draw();
        });
        elem.on('dragend', (e: any) => { // dragged
            let scaleLayer = 0;
            switch (true) {
                case +this.layer.scaleY().toFixed() <= 3:
                    scaleLayer = 1;
                    break;
                default:
                    scaleLayer = 2;
                    break;
            }

            elem.x(+(+elem.x().toFixed(scaleLayer) / 5).toFixed(scaleLayer - 1) * 5);
            elem.y(+(+elem.y().toFixed(scaleLayer) / 5).toFixed(scaleLayer - 1) * 5);

            this.detectCollision(this.calculate(), this.layer, this.objectPoint({
                height: elem.height(),
                width: elem.width(),
                x: elem.x(),
                y: elem.y(),
                rotate: elem.rotation(),
                id: elem.id(),
                name: elem.name()
            }));
            try {
                const TEXT = this.layer.findOne(`#${cell.WMS_ID}`);
                TEXT.x(elem.x() + elem.height() * Math.sin((elem.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                TEXT.y(elem.y() - elem.height() * Math.cos((elem.rotation() / (180 / Math.PI)) + 1.5708 * 2));
            } catch { }

            textIdCells.hide();
            this.stage.draw();
        });

        const textIdCells = new Konva.Text({
            text: '',
            fontFamily: 'Calibri',
            fontSize: 12 / SCALE,
            padding: 5 / SCALE,
            textFill: 'white',
            fill: 'black',
            alpha: 0.75,
            visible: false,
        });
        // draw id cells after cursor. (Toggle show)
        // elem.on('mousemove', () => {
        //     textIdCells.position({
        //         x: (this.stage.getPointerPosition().x + 5 - this.layer.x()) / this.layer.scaleX(),
        //         y: (this.stage.getPointerPosition().y + 5 - this.layer.y()) / this.layer.scaleX()
        //     });
        //     textIdCells.setText(cell.AX_ID);
        //     textIdCells.fontSize(12 / this.layer.scaleX());
        //     textIdCells.padding(5 / this.layer.scaleX());
        //     textIdCells.show();
        //     this.layer.add(textIdCells);
        //     this.layer.draw();
        // });
        // elem.on('mouseout', () => {
        //     textIdCells.hide();
        //     this.stage.draw();
        // })

        elem.on('mouseenter', () =>
            this.stage.container().style.cursor = 'pointer'
        );
        elem.on('mouseleave', () =>
            this.stage.container().style.cursor = 'default'
        );
        return elem;
    }


    group(id: string, wms_id: number, array: Cell[], callbackObject?: CallBackObj): Konva.Group {  // создание группы

        let gr = new Konva.Group({
            id: '' + wms_id,
            x: 100,
            y: 100,
            draggable: this.checkUserAdmin(),    // условие admin/user массив, так как может быть множество имен
            stroke: 'black',
            strokeWidth: 4,
            fill: 'red',
            name: id,
            check: true,
            WMS_ID: wms_id
        });

        const textIdCells = new Konva.Text({
            text: id,
            fontFamily: 'Calibri',
            fontSize: 12 / SCALE,
            padding: 5 / SCALE,
            textFill: 'white',
            fill: 'black',
            alpha: 0.75,
            visible: false,
        });
        this.layer.add(textIdCells);
        this.layer.draw();


        array.sort((a, b) => {
            if (a.AX_ID < b.AX_ID) {
                return -1;
            }
            if (a.AX_ID > b.AX_ID) {
                return 1;
            }
            return 0;
        }).map((cell: Cell, index: number) => {
            if (index % 2) {
                let recRight = new Konva.Rect({
                    x: 0 + 70,
                    y: 0 + (index - 1) * 5,
                    height: 5,
                    width: 50,
                    fill: 'grey',
                    id: '' + cell.WMS_ID,
                    WMS_ID: cell.WMS_ID,
                });
                recRight.on('click', (e: any) => {
                    // this.arrayRect.map((rect: Konva.Rect) => {rect.attrs.fill = 'grey'});
                    this.arrayRect.map((gr: Konva.Group) => gr.children.toArray().map((rect: Konva.Rect) => { rect.attrs.fill = 'grey' }));
                    recRight.fill('red');
                    this.layer.draw();
                });
                gr.add(this.builGroupRect(cell, recRight));
            } else {
                let recLeft = new Konva.Rect({
                    x: 0,
                    y: 0 + index * 5,
                    height: 5,
                    width: 50,
                    fill: 'grey',
                    id: '' + cell.WMS_ID,
                    WMS_ID: cell.WMS_ID,
                });
                recLeft.on('click', (e: any) => {
                    // this.arrayRect.map((rect: Konva.Rect) => {rect.attrs.fill = 'grey'});
                    this.arrayRect.map((gr: Konva.Group) => gr.children.toArray().map((rect: Konva.Rect) => { rect.attrs.fill = 'grey' }));
                    recLeft.fill('red');
                    this.layer.draw();
                });
                gr.add(this.builGroupRect(cell, recLeft));
            }
        });

        // const x = gr.x() + gr.children[0].x();
        // const y = gr.y() + gr.children[0].y();
        gr.on(callbackObject.event, callbackObject.func);

        if (gr.children.length % 2) {
            if (gr.children.length === 1) {
                gr.width(gr.children[0].x() + gr.children[0].width());
                gr.height(gr.children[0].height());
            } else {
                gr.width(gr.children[1].x() + gr.children[1].width());
                gr.height(gr.children[gr.children.length - 1].y());
            }
        } else {
            gr.width(gr.children[gr.children.length - 1].x() + gr.children[gr.children.length - 1].width());
            gr.height(gr.children[gr.children.length - 1].y());
        };
        // gr.width(gr.children[1].x() + gr.children[1].width())
        // gr.height(gr.children[1].y() + gr.children[1].height())


        // const tr = new Konva.Transformer({
        //     node: gr
        //   });


        // dragged ? (this.arrayRect.push(gr)) : null;
        this.arrayRect.push(gr)
        return gr;
    }


    groupDb(cellGr: GroupCell, arrayCell: Cell[], callbackObject?: CallBackObj): Konva.Group {

        const MARGIN_WIDTH = cellGr.WIDTH - WIDTH_GROUP_CELL * (cellGr.WIDTH / WIDTH_GROUP) * 2;
        const WIDTH_DB_CELL = (cellGr.WIDTH - (cellGr.WIDTH - WIDTH_GROUP_CELL * (cellGr.WIDTH / WIDTH_GROUP) * 2)) / 2;

        let items = Math.ceil(arrayCell.length / 2);
        const MARGIN_HEIGTH = (cellGr.HEIGHT - HEIGTH_GROUP_CELL * (cellGr.HEIGHT / HEIGTH_GROUP) * items) / items;
        const HEIGTH_DB_CELL = cellGr.HEIGHT / (arrayCell.length - 1)

        const textIdCells = new Konva.Text({
            text: cellGr.AX_ID,
            fontFamily: 'Calibri',
            fontSize: 12 / SCALE,
            padding: 5 / SCALE,
            textFill: 'white',
            fill: 'black',
            alpha: 0.75,
            visible: false,
        });

        // this.layer.add(textIdCells);
        // this.layer.draw();

        const option = {
            id: '' + cellGr.WMS_ID,
            x: cellGr.M_X,
            y: cellGr.M_Y,
            draggable: this.checkUserAdmin(),
            stroke: 'black',
            strokeWidth: 4,
            fill: 'red',
            name: cellGr.AX_ID,
            check: true,
            WMS_ID: cellGr.WMS_ID
        }
        if (cellGr.WIDTH) {
            Object.defineProperty(option, 'width', {
                value: cellGr.WIDTH,
                configurable: true,
                enumerable: true
            });
            option.check = false;
        }
        if (cellGr.HEIGHT) {
            Object.defineProperty(option, 'height', {
                value: cellGr.HEIGHT,
                configurable: true,
                enumerable: true
            });
        }
        if (cellGr.ANGLE) {
            Object.defineProperty(option, 'rotation', {
                value: cellGr.ANGLE,
                configurable: true,
                enumerable: true
            });
        }

        let gr = new Konva.Group(option);
        this.addNameCellToField(cellGr);

        gr.on('transformend', (e: any) => {
            try {   // установить параметры ax_id ячейки 
                const TEXT: Konva.Text = this.layer.findOne(`#${cellGr ? cellGr.WMS_ID : gr.id()}`);
                TEXT.x(gr.x() + gr.height() * Math.sin((gr.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                TEXT.y(gr.y() - gr.height() * Math.cos((gr.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                TEXT.rotation(gr.rotation());
                // const s: number = elem.scaleX()
                // TEXT.fontSize(TEXT.fontSize() * gr.scaleX());
            } catch { }
            this.layer.draw()
            this.detectCollision(this.calculate(), this.layer, this.objectPoint({
                height: gr.height(),
                width: gr.width(),
                x: gr.x(),
                y: gr.y(),
                rotate: gr.rotation(),
                id: gr.id(),
                name: gr.name()
            }));
            textIdCells.hide();
            this.stage.draw();
        });
        gr.on('dragend', (e: any) => { // dragged
            let scaleLayer = 0;
            switch (true) {
                case +this.layer.scaleY().toFixed() <= 3:
                    scaleLayer = 1;
                    break;
                default:
                    scaleLayer = 2;
                    break;
            }

            gr.x(+(+gr.x().toFixed(scaleLayer) / 5).toFixed(scaleLayer - 1) * 5);
            gr.y(+(+gr.y().toFixed(scaleLayer) / 5).toFixed(scaleLayer - 1) * 5);

            this.detectCollision(this.calculate(), this.layer, this.objectPoint({
                height: gr.height(),
                width: gr.width(),
                x: gr.x(),
                y: gr.y(),
                rotate: gr.rotation(),
                id: gr.id(),
                name: gr.name()
            }));
            try {
                const TEXT: Konva.Text = this.layer.findOne(`#${cellGr ? cellGr.WMS_ID : gr.id()}`);
                TEXT.x(gr.x() + gr.height() * Math.sin((gr.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                TEXT.y(gr.y() - gr.height() * Math.cos((gr.rotation() / (180 / Math.PI)) + 1.5708 * 2));
            } catch (err) {
                console.log(gr);
                console.log(cellGr);
                console.log(err);
            }

            textIdCells.hide();
            this.stage.draw();
        });



        gr.on(callbackObject.event, callbackObject.func);
        arrayCell.sort((a, b) => {
            if (a.AX_ID < b.AX_ID) {
                return -1;
            }
            if (a.AX_ID > b.AX_ID) {
                return 1;
            }
            return 0;
        }).map((cell: Cell, index: number) => {
            if (index % 2) {
                const REC_RIGHT = new Konva.Rect({
                    x: isNaN(WIDTH_DB_CELL + MARGIN_WIDTH) ? 0 + 70 : WIDTH_DB_CELL + MARGIN_WIDTH,       // из-за отступов нужно рассчитывать так
                    //!!! ОТСТУПЫ ПО ВЕРТИКАЛИ
                    y: isNaN((index - 1) * (HEIGTH_DB_CELL)) ? 0 + (index - 1) * 5 : (index - 1) * (HEIGTH_DB_CELL), //+ (HEIGTH_DB_CELL + MARGIN_HEIGTH),    //!!! ОТСТУПЫ ПО ВЕРТИКАЛИ
                    //!!! ОТСТУПЫ ПО ВЕРТИКАЛИ
                    height: isNaN(HEIGTH_DB_CELL) ? 5 : HEIGTH_DB_CELL,
                    width: isNaN(WIDTH_GROUP_CELL * (cellGr.WIDTH / WIDTH_GROUP)) ? 50 : WIDTH_GROUP_CELL * (cellGr.WIDTH / WIDTH_GROUP),
                    fill: 'grey',
                    id: cell.AX_ID,
                    WMS_ID: cell.WMS_ID
                });
                

                REC_RIGHT.on('click', (e: any) => {
                    // this.arrayRect.map((rect: Konva.Rect) => {rect.attrs.fill = 'grey'});
                    this.arrayRect.map((gr: Konva.Group) => gr.children.toArray().map((rect: Konva.Rect) => { rect.attrs.fill = 'grey' }));
                    REC_RIGHT.fill('red');
                    this.layer.draw();
                });
                gr.add(this.builGroupRect(cell, REC_RIGHT));
            } else {
                const REC_LEFT = new Konva.Rect({
                    x: 0,
                    y: isNaN(index * (HEIGTH_DB_CELL)) ? 0 + index * 5 : index * (HEIGTH_DB_CELL),
                    height: isNaN(HEIGTH_DB_CELL) ? 5 : HEIGTH_DB_CELL,
                    width: isNaN(WIDTH_GROUP_CELL * (cellGr.WIDTH / WIDTH_GROUP)) ? 50 : WIDTH_GROUP_CELL * (cellGr.WIDTH / WIDTH_GROUP),
                    fill: 'grey',
                    id: cell.AX_ID,
                    WMS_ID: cell.WMS_ID
                });
                REC_LEFT.on('click', (e: any) => {
                    this.arrayRect.map((gr: Konva.Group) => gr.children.toArray().map((rect: Konva.Rect) => { rect.attrs.fill = 'grey' }));
                    REC_LEFT.fill('red');
                    this.layer.draw();
                });
                gr.add(this.builGroupRect(cell, REC_LEFT));
            }
        });

        if (gr.children.length % 2) {
            if (gr.children.length === 1) {
                gr.width(gr.children[0].x() + gr.children[0].width());
                gr.height(gr.children[0].height());
            } else {
                gr.width(gr.children[1].x() + gr.children[1].width());
                gr.height(gr.children[gr.children.length - 1].y());
            }
        } else {
            gr.width(gr.children[gr.children.length - 1].x() + gr.children[gr.children.length - 1].width());
            gr.height(gr.children[gr.children.length - 1].y() + gr.children[gr.children.length - 1].height());
        };

        gr = this.addEventGroup(gr); // #refact
        this.arrayRect.push(gr);
        return gr;
        // this.layer.add(gr);
        // this.layer.draw();
    }



    addNewRect(id: string, wms_id: number, x: number, y: number, width: number, height: number): Konva.Rect {
        this.addNameCellToField(null, {           // #говнокод-12
            id,
            wms_id,
            x,
            y,
            width,
            height
        });
        return new Konva.Rect({
            x: (x + Math.abs(this.layer.x())) / (this.layer.scaleX() === 0 ? 1 : this.layer.scaleX()),
            y: (y + Math.abs(this.layer.y())) / (this.layer.scaleY() === 0 ? 1 : this.layer.scaleY()),
            rotation: 0,
            width: width / (this.layer.scaleX() === 0 ? 1 : this.layer.scaleX()),
            height: height / (this.layer.scaleY() === 0 ? 1 : this.layer.scaleY()),
            draggable: this.checkUserAdmin(),
            fill: 'grey',
            check: true,
            id: '' + wms_id,
            name: id,
            WMS_ID: +document.querySelector(`#${id}`).getAttribute('data-id')
        })
    }
    addDBRect(cell: Cell): Konva.Rect {
        this.addNameCellToField(cell);
        return new Konva.Rect({
            x: cell.M_X,
            y: cell.M_Y,
            rotation: cell.ANGLE,
            width: cell.WIDTH,
            height: cell.HEIGHT,
            draggable: this.checkUserAdmin(),
            fill: 'grey',
            check: false,
            id: this.generName(),
            name: cell.NAME,
            WMS_ID: cell.WMS_ID
        })
    }

    addNameCellToField(cell: Cell, ...arg: any[]): void {
        // x: 
        // y: rect.y - rect.width * Math.cos((rect.rotate / (180 / Math.PI)) + 1.5708)
        let option = {};
        if (cell) {
            option = {
                // rect.x + rect.height * Math.sin((rect.rotate / (180 / Math.PI)) + 1.5708 * 2)
                x: (cell.M_X ?? 1) + (cell.HEIGHT ?? 10) * Math.sin(((cell.ANGLE ?? 0) / (180 / Math.PI)) + 1.5708 * 2),
                y: (cell.M_Y ?? 1) - (cell.HEIGHT ?? 10) * Math.cos(((cell.ANGLE ?? 0) / (180 / Math.PI)) + 1.5708 * 2),
                text: cell.AX_ID,
                id: '' + cell.WMS_ID,
                fontFamily: 'Calibri',
                fontSize: 16,
                padding: 5 / SCALE,
                textFill: 'white',
                fill: 'black',
                alpha: 0.75,
                visible: true,
                rotation: (cell.ANGLE ?? 0)
            }
        } else {
            option = {
                // rect.x + rect.height * Math.sin((rect.rotate / (180 / Math.PI)) + 1.5708 * 2)
                x: arg[0].x + arg[0].height * Math.sin((0 / (180 / Math.PI)) + 1.5708 * 2),
                y: arg[0].y - arg[0].height * Math.cos((0 / (180 / Math.PI)) + 1.5708 * 2),
                text: arg[0].id,
                id: '' + arg[0].wms_id,
                fontFamily: 'Calibri',
                fontSize: 16,
                padding: 5 / SCALE,
                textFill: 'white',
                fill: 'black',
                alpha: 0.75,
                visible: true,
                rotation: 0
            }
        }
        const TEXT = new Konva.Text(option);
        this.layer.add(TEXT);
        this.layer.draw();

    }

    /**
     * @param  {string} id This AX_ID. Name cells example - 'ot-vs1-**-**-***'
     * @param  {Cell} cell? Object cell
     * @returns void
     */
    addRect(id: string, wms_id: number, cell?: Cell, callbackObject?: CallBackObj): void {        // add new rect to layout and array
        let elem =
            cell ?
                this.addDBRect(cell) :
                this.addNewRect(id, wms_id, 10, 10, 100, 50); //
        // this.addNameCellToField(cell);


        elem.on('click', (e: any) => {
            // this.arrayRect.map((rect: Konva.Rect) => {rect.attrs.fill = 'grey'});
            this.arrayRect.map((gr: Konva.Group) => gr.children.toArray().map((rect: Konva.Rect) => { rect.attrs.fill = 'grey' }));
            elem.fill('red');
            this.layer.draw();
        });

        // this.arrayRect.push(elem);
        if (this.checkUserAdmin()) {
            elem.on('click', (e: any) => {
                this.transformElements.nodes([e.target]);
                this.layer.draw();
            });
        }

        elem.on('transformend', (e: any) => {
            elem.rotation(+(elem.rotation() / 15).toFixed() * 15) // grid  установить угол по сетке с раз. в 15
            elem.width(elem.scale().x * elem.width())           // относительно масвштаба изменить ширину и длину
            elem.height(elem.scale().y * elem.height())
            try {   // установить параметры ax_id ячейки 
                const TEXT: Konva.Text = this.layer.findOne(`#${cell ? cell.WMS_ID : elem.id()}`);
                TEXT.x(elem.x() + elem.height() * Math.sin((elem.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                TEXT.y(elem.y() - elem.height() * Math.cos((elem.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                TEXT.rotation(elem.rotation());
                // const s: number = elem.scaleX()
                // TEXT.fontSize(TEXT.fontSize() * elem.scaleX());
            } catch { }
            elem.scale({ x: 1, y: 1 })
            this.layer.draw()
            this.detectCollision(this.calculate(), this.layer, this.objectPoint({
                height: elem.height(),
                width: elem.width(),
                x: elem.x(),
                y: elem.y(),
                rotate: elem.rotation(),
                id: elem.id(),
                name: elem.name()
            }));


            textIdCells.hide();
            this.stage.draw();
        });
        elem.on('dragend', (e: any) => { // dragged
            let scaleLayer = 0;
            switch (true) {
                case +this.layer.scaleY().toFixed() <= 3:
                    scaleLayer = 1;
                    break;
                default:
                    scaleLayer = 2;
                    break;
            }

            elem.x(+(+elem.x().toFixed(scaleLayer) / 5).toFixed(scaleLayer - 1) * 5);
            elem.y(+(+elem.y().toFixed(scaleLayer) / 5).toFixed(scaleLayer - 1) * 5);

            this.detectCollision(this.calculate(), this.layer, this.objectPoint({
                height: elem.height(),
                width: elem.width(),
                x: elem.x(),
                y: elem.y(),
                rotate: elem.rotation(),
                id: elem.id(),
                name: elem.name()
            }));
            try {
                const TEXT: Konva.Text = this.layer.findOne(`#${cell ? cell.WMS_ID : elem.id()}`);
                TEXT.x(elem.x() + elem.height() * Math.sin((elem.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                TEXT.y(elem.y() - elem.height() * Math.cos((elem.rotation() / (180 / Math.PI)) + 1.5708 * 2));
            } catch (err) {
                console.log(elem);
                console.log(cell);
                console.log(err);
            }

            textIdCells.hide();
            this.stage.draw();
        });

        const textIdCells = new Konva.Text({
            text: '',
            fontFamily: 'Calibri',
            fontSize: 12 / SCALE,
            padding: 5 / SCALE,
            textFill: 'white',
            fill: 'black',
            alpha: 0.75,
            visible: false,
        });

        // draw id cells after cursor. (Toggle show)
        // elem.on('mousemove', () => {
        //     textIdCells.position({
        //         x: (this.stage.getPointerPosition().x + 5 - this.layer.x()) / this.layer.scaleX(),
        //         y: (this.stage.getPointerPosition().y + 5 - this.layer.y()) / this.layer.scaleX()
        //     });

        //     textIdCells.setText(cell ? cell.WMS_ID : elem.id());
        //     textIdCells.fontSize(12 / this.layer.scaleX());
        //     textIdCells.padding(5 / this.layer.scaleX());
        //     textIdCells.show();
        //     this.layer.add(textIdCells);
        //     this.layer.draw();
        // });
        // elem.on('mouseout', () => {
        //     textIdCells.hide();
        //     this.stage.draw();
        // })

        if (this.checkUserAdmin()) {
            elem.on('dblclick', () => {
                // this.arrayRect.splice(this.arrayRect.indexOf(elem), 1); // добавить в группу
                elem.fire('mouseout');                                  // dispatche event
                this.newOption(elem);
                elem.remove();
                this.transformElements.nodes([]);
                this.stage.container().style.cursor = 'default';
            })
            elem.on('mouseenter', () =>
                this.stage.container().style.cursor = 'pointer'
            );
            elem.on('mouseleave', () =>
                this.stage.container().style.cursor = 'default'
            );
        }



        if (callbackObject) { // add custom
            elem.on(callbackObject.event, callbackObject.func);
        }

        this.layer.add(elem);
        this.layer.draw();
        this.stage.draw();
    }


    addEventGroup(group: Konva.Group): Konva.Group {
        group.on('transformstart', (e: any) => {
            console.log(group.width());
        });
        group.on('transformend', (e: any) => {
            (<Collection<Konva.Rect>>group.children).each(chiledren => {
                chiledren.y(chiledren.y() * group.scaleY());
                chiledren.x(chiledren.x() * group.scaleX());
                chiledren.width(chiledren.width() * group.scaleX());
                chiledren.height(chiledren.height() * group.scaleY());
            });

            group.scale({ x: 1, y: 1 });
            if (group.children.length % 2) {
                if (group.children.length === 1) {
                    group.width(group.children[0].x() + group.children[0].width())    // set width, heigth group
                    group
                        .height(
                            group.children[0].height()
                        )    // set width, heigth group
                } else {
                    group.width(group.children[1].x() + group.children[1].width())    // set width, heigth group
                    group.height(group.children[group.children.length - 1].y());
                }

            } else {
                group
                    .width(
                        group.children[group.children.length - 1].x()
                        + group.children[group.children.length - 1].width()
                    )    // set width, heigth group
                group
                    .height(
                        group.children[group.children.length - 1].y()
                        + group.children[group.children.length - 1].height()
                    )    // set width, heigth group
            };
            this.detectCollision(this.calculate(), this.layer, this.objectPoint({
                height: group.height(),
                width: group.width(),
                x: group.x(),
                y: group.y(),
                rotate: group.rotation(),
                id: group.id(),
                name: group.name()
            }));
            textIdCells.hide();
            this.stage.draw();
            // const textIdCells = new Konva.Text({
            //     text: '',
            //     fontFamily: 'Calibri',
            //     fontSize: 12 / SCALE,
            //     padding: 5 / SCALE,
            //     textFill: 'white',
            //     fill: 'black',
            //     alpha: 0.75,
            //     visible: false,
            // });
        });
        if (this.checkUserAdmin()) {
            group.on('dblclick', () => {
                this.arrayRect.splice(this.arrayRect.indexOf(group), 1);
                group.fire('mouseout');
                this.newOption(group);
                group.remove();
                this.transformElements.nodes([]);
                this.stage.container().style.cursor = 'default';
            });
            group.on('click', () => {
                this.transformElements.nodes([group]);
            });
        }


        // group.on('click', callbackObject.func);

        group.on('dragend', () => { // dragged
            this.detectCollision(this.calculate(), this.layer, this.objectPoint({
                height: group.height(),
                width: group.width(),
                x: group.x(),
                y: group.y(),
                rotate: group.rotation(),
                id: group.id(),
                name: group.name()
            }));
            try {   // установить параметры ax_id ячейки 
                // const elem = group;
                // const TEXT: Konva.Text = this.layer.findOne(`#${cellGr ? cellGr.WMS_ID : elem.id()}`);
                // TEXT.x(elem.x() + elem.height() * Math.sin((elem.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                // TEXT.y(elem.y() - elem.height() * Math.cos((elem.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                // TEXT.rotation(elem.rotation());
                // const s: number = elem.scaleX()
                // TEXT.fontSize(TEXT.fontSize() * elem.scaleX());
            } catch { }
            // textIdCells.hide();
            this.stage.draw();
        });

        const textIdCells = new Konva.Text({
            text: '',
            fontFamily: 'Calibri',
            fontSize: 12,
            padding: 5,
            textFill: 'white',
            fill: 'black',
            alpha: 0.75,
            visible: false,
        });
        return group;
    }



    addEventGroupKulla(group: Konva.Group): Konva.Group {
        group.on('transformstart', (e: any) => {
            console.log(group.width());
        });


        group.on('transformend', (e: any) => {
            // try {   // установить параметры ax_id ячейки 
            //     const TEXT: Konva.Text = this.layer.findOne(`#${group.id()}`);
            //     TEXT.x(group.x() + group.height() * Math.sin((group.rotation() / (180 / Math.PI)) + 1.5708 * 2));
            //     TEXT.y(group.y() - group.height() * Math.cos((group.rotation() / (180 / Math.PI)) + 1.5708 * 2));
            //     TEXT.rotation(group.rotation());
            //     // const s: number = elem.scaleX()
            //     // TEXT.fontSize(TEXT.fontSize() * gr.scaleX());
            // } catch { }
            // this.layer.draw();
            // this.detectCollision(this.calculate(), this.layer, this.objectPoint({
            //     height: group.height(),
            //     width: group.width(),
            //     x: group.x(),
            //     y: group.y(),
            //     rotate: group.rotation(),
            //     id: group.id(),
            //     name: group.name()
            // }));
            // textIdCells.hide();
            // this.stage.draw();
        });
        group.on('dragend', (e: any) => { // dragged
            let scaleLayer = 0;
            switch (true) {
                case +this.layer.scaleY().toFixed() <= 3:
                    scaleLayer = 1;
                    break;
                default:
                    scaleLayer = 2;
                    break;
            }

            group.x(+(+group.x().toFixed(scaleLayer) / 5).toFixed(scaleLayer - 1) * 5);
            group.y(+(+group.y().toFixed(scaleLayer) / 5).toFixed(scaleLayer - 1) * 5);

            this.detectCollision(this.calculate(), this.layer, this.objectPoint({
                height: group.height(),
                width: group.width(),
                x: group.x(),
                y: group.y(),
                rotate: group.rotation(),
                id: group.id(),
                name: group.name()
            }));
            try {
                const TEXT: Konva.Text = this.layer.findOne(`#${group.id()}`);
                TEXT.x(group.x() + group.height() * Math.sin((group.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                TEXT.y(group.y() - group.height() * Math.cos((group.rotation() / (180 / Math.PI)) + 1.5708 * 2));
            } catch (err) {
                console.log(err);
            }
            console.log(textIdCells);

            textIdCells.hide();
            this.stage.draw();
        });

        group.on('transformend', (e: any) => {
            (<Collection<Konva.Rect>>group.children).each(chiledren => {
                chiledren.y(chiledren.y() * group.scaleY());
                chiledren.x(chiledren.x() * group.scaleX());
                chiledren.width(chiledren.width() * group.scaleX());
                chiledren.height(chiledren.height() * group.scaleY());
            });

            group.scale({ x: 1, y: 1 });

            group.width(group.children[0].x() + group.children[0].width())
            group.height(group.children[0].height() * group.children.length * 2)   // set width, heigth group

            // if (group.children.length) {
            //     if (group.children.length === 1) {
            //         group.width(group.children[0].x() + group.children[0].width())    // set width, heigth group
            //         group.height(group.children[0].height())    // set width, heigth group
            //     } else {
            //         group.width(group.children[1].x() + group.children[1].width())    // set width, heigth group
            //         group.height(group.children.length * group.children[0].height());
            //     }

            // } else {
            //     group
            //         .width(
            //             group.children[group.children.length - 1].x()
            //             + group.children[group.children.length - 1].width()
            //         )    // set width, heigth group
            //     group
            //         .height(
            //             group.children[group.children.length - 1].y()
            //             + group.children[group.children.length - 1].height()
            //         )    // set width, heigth group
            // };
            try {   // установить параметры ax_id ячейки 
                const TEXT: Konva.Text = this.layer.findOne(`#${group.id()}`);
                TEXT.x(group.x() + group.height() * Math.sin((group.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                TEXT.y(group.y() - group.height() * Math.cos((group.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                TEXT.rotation(group.rotation());
                // const s: number = elem.scaleX()
                // TEXT.fontSize(TEXT.fontSize() * gr.scaleX());
            } catch { }
            this.detectCollision(this.calculate(), this.layer, this.objectPoint({
                height: group.height(),
                width: group.width(),
                x: group.x(),
                y: group.y(),
                rotate: group.rotation(),
                id: group.id(),
                name: group.name()
            }));
            textIdCells.position({
                x: group.x(),
                y: group.y() + group.height() + HEIGTH_GROUP_CELL
            });

            this.layer.draw();
            this.stage.draw();
            // const textIdCells = new Konva.Text({
            //     text: '',
            //     fontFamily: 'Calibri',
            //     fontSize: 12 / SCALE,
            //     padding: 5 / SCALE,
            //     textFill: 'white',
            //     fill: 'black',
            //     alpha: 0.75,
            //     visible: false,
            // });
        });
        if (this.checkUserAdmin()) {
            group.on('dblclick', () => {
                this.arrayRect.splice(this.arrayRect.indexOf(group), 1);
                group.fire('mouseout');
                this.newOption(group);
                group.remove();
                this.transformElements.nodes([]);
                this.stage.container().style.cursor = 'default';
            });
            group.on('click', () => {
                this.transformElements.nodes([group]);
            });
        }


        // group.on('click', callbackObject.func);

        group.on('dragend', () => { // dragged
            this.detectCollision(this.calculate(), this.layer, this.objectPoint({
                height: group.height(),
                width: group.width(),
                x: group.x(),
                y: group.y(),
                rotate: group.rotation(),
                id: group.id(),
                name: group.name()
            }));
            try {   // установить параметры ax_id ячейки 
                // const elem = group;
                // const TEXT: Konva.Text = this.layer.findOne(`#${cellGr ? cellGr.WMS_ID : elem.id()}`);
                // TEXT.x(elem.x() + elem.height() * Math.sin((elem.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                // TEXT.y(elem.y() - elem.height() * Math.cos((elem.rotation() / (180 / Math.PI)) + 1.5708 * 2));
                // TEXT.rotation(elem.rotation());
                // const s: number = elem.scaleX()
                // TEXT.fontSize(TEXT.fontSize() * elem.scaleX());
            } catch { }
            // textIdCells.hide();
            this.stage.draw();
        });

        const textIdCells = new Konva.Text({
            text: '',
            fontFamily: 'Calibri',
            fontSize: 12,
            padding: 5,
            textFill: 'white',
            fill: 'black',
            alpha: 0.75,
            visible: false,
        });
        return group;
    }


    addGroup(id: string, wms_id: number, array: Cell[], callbackObject?: CallBackObj): void {
        let group = this.group(id, wms_id, array, callbackObject);

        this.arrayRenderedElement.push(group);

        group = this.addEventGroup(group);
        // draw id cells after cursor. (Toggle show)

        this.layer.add(group);
        this.layer.draw();
    }

    generName(): string {
        return 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'.replace(/[x]/g, x => {
            let r = +(Math.random() * 16 || 0).toFixed();
            let v = x == 'x' ? r : (r && 4 || 6);
            return v.toString(16);
        })
    }
    /**
     * @param  {Konva.Rect|Konva.Group} val
     * @returns void
     * check element class
     */
    public callBackAddEvent4Proto(val: Konva.Rect | Konva.Group): void;
    public callBackAddEvent4Proto(val: any, ...args: any[]): void {
        switch (true) {
            case (val instanceof Konva.Rect):
                // this.addRect(val)
                break;
            case (val instanceof Konva.Group):
                // this.addGroup(val)
                break;
            default:
                break;
        }
    }

    calculate(): Array<Calculate> {

        return this.arrayRect.map((val: Konva.Rect | Konva.Group) =>
            Object({
                height: val.height(),
                width: val.width(),
                x: val.x(),
                y: val.y(),
                rotate: val.rotation(),
                id: val.id()
            })
        ).map((val: Calculate) => this.objectPoint(val))
            .map(val => Object({ ...val.vect, id: val.id }));
    }

    addEvent4GroupOrRect(val: Konva.Rect | Konva.Group): void;
    addEvent4GroupOrRect(val: Konva.Group): void;
    addEvent4GroupOrRect(val: any, ...args: any[]): void {

    }


    addedEvents(val: Konva.Rect | Konva.Group): void;
    addedEvents(val: any, ...args: any[]): void {
        val.on('click tap', (e: any) => {
            this.callBackAddEvent4Proto(val)
        });
    }


    zoom(arrayCells: Array<Konva.Rect | Konva.Group>) {
        let MIN_X = arrayCells[0]?.x();
        let MIN_Y = arrayCells[0]?.y();
        let MAX_X = arrayCells[0]?.x();
        let MAX_Y = arrayCells[0]?.y();
        let WIDTH = arrayCells[0]?.width();

        arrayCells.map((val: Konva.Rect) => {
            MIN_X = val.x() < MIN_X ? val.x() : MIN_X;
            MIN_Y = val.y() < MIN_Y ? val.y() : MIN_Y;
            MAX_X = val.x() > MAX_X ? val.x() : MAX_X;
            MAX_Y = val.y() > MAX_Y ? val.y() : MAX_Y;
            WIDTH = val.width() > WIDTH ? val.width() : WIDTH;
        });


        const DIST = Math.sqrt(Math.pow((MAX_X - MIN_X), 2) + Math.pow((MAX_Y - MIN_Y), 2));

        this.layer.position({
            x: -(MIN_X * this.layer.width() / (DIST + WIDTH)) + WIDTH, //(MAX_X / MIN_X)),
            y: -(MIN_Y * this.layer.width() / (DIST + WIDTH)) + WIDTH  //(MAX_Y / MIN_Y))
        });
        this.layer.scale({
            x: this.layer.width() / (DIST + WIDTH),
            y: this.layer.width() / (DIST + WIDTH)
        });
        this.layer.draw();

    }

    clear() {
        this.stage.clear();
        this.layer.clear();
        this.imageKonva = null;
        this.image = null;
        this.arrayRect = [];
        this.arrayRenderedElement = [];
        this.layer = new Konva.Layer();
    }

    factory(cell: Cell, array: Cell[], callbackObject?: CallBackObj): Promise<any> {
        // console.log(cell);
        // console.log(array);
        return new Promise(res => {
            setTimeout(() => {
                let group: Konva.Group;
                if (this.TYPE_OBJECT_FACTORY.hasOwnProperty(cell.TYPE_NAME)) {
                    group = this
                        .TYPE_OBJECT_FACTORY[cell.TYPE_NAME]
                        .call(this, cell, array, callbackObject);
                } else {
                    group = this
                        .TYPE_OBJECT_FACTORY['default']
                        .call(this, cell, array, callbackObject);
                }

                this.arrayRenderedElement.push(group);
                this.layer.add(group);
                this.layer.draw();
                res(this.arrayRect);
            }, 0);
        });
    }

    checkUserAdmin(): boolean {
        return JSON.parse(localStorage.getItem('ug')).includes(ADMIN_GROUP);
    }
}


