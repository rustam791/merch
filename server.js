const express = require('express');
const formidable = require('formidable');
const bodyParser = require('body-parser');
const server = express();
const { renameSync, existsSync, exists, rename } = require('fs');
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({
    extended: true
}));
server.use(express.static('./uploads'));
server.use(express.static('./fonts'));
server.use(express.static('./img'));
server.set('etag', false)
server.use((req, res, next) => {
    res.set('Cache-Control', 'no-store')
    next()
})
server.use(express.static('./uploads'))
server.listen(3000, () => console.log('3000'));

/**
 * @param  {} '/check_plan'
 * @param  {} req
 * @param  {} res
 */
server.post('/exist_image_plan', (req, res) => {
    try {
        exists(`./uploads/${req.body.fileName}`, check => {
            res.json({check});
        });
    } catch (error) {
        res.json({error});
    }
});


/**
 * @param  {} '/save_image'
 * @param  {} req
 * @param  {} res
 */
server.post('/save_image', (req, res) => {
    try {
        const form = formidable({ multiples: true, uploadDir: './uploads/' });
        form.parse(req, (err, fields, files) => {
            if (!fields?.fileName) {
                res.status(404).json({ fileName: 'error' });
                return;
            }
            renameSync(files.file.path, `${files.file.path}-${fields.fileName}`);
            res.json({ name: `${files.file.path.split('\/')[1]}-${fields.fileName}` });
        });
    } catch (error) {
        res.json({error})
    }
});

server.get('/', (req, res) => {
    res.status(200);
    res.sendFile(__dirname + '/index.html');
});
server.get('/dist/bundle.js', (req, res) => {
    res.status(200);
    res.sendFile(__dirname + '/dist/bundle.js');
});
server.get('/dist/ui.js', (req, res) => {
    res.sendFile(__dirname + '/dist/ui.js');
});
server.get('/plan.png', (req, res) => {
    res.status(200);
    res.sendFile(__dirname + '/plan.png');
});
server.get('/style.css', (req, res) => {
    res.status(200);
    res.sendFile(__dirname + '/style.css');
});

// var http = require('http');
// const { createWriteStream, writeFileSync, readFileSync } = require('fs');
// const { type } = require('os');
// http.createServer(function (req, res) {
//     if (req.url === '/save_image') {
//         var form = new formidable.IncomingForm({
//             uploadDir: './uploads/'
//         });
//         // let stream = createWriteStream('new.jpg');
//         form.parse(req, function (err, fields, files) {

//             let pm = new Blob([files.file], { type: "image/png" });
//             // readFileSync(files)
//             writeFileSync('tss.jpg', pm);
//             // stream.pipe();
//             res.write('File uploaded');
//             res.end();
//         });
//     } else {
//         res.writeHead(200, { 'Content-Type': 'text/html' });
//         res.write('<p>it`s trap</p>');

//         return res.end();
//     }
// }).listen(8080);