-- EXAMPLE SQL SCRIPTS
SELECT
    JSON_ARRAYAGG(JSON_OBJECT(
    'WMS_ID' is aw.ID,
    aw.AX_ID, 
    aw.AX_REC_ID, 
    --aw.AX_MODDIF_DATE, 
    --aw.AX_CREATE_DATE,
    aw.INVENT_LOCATION_ID,
    'TYPE_NAME' is inv_loc.AX_ID,
    'TYPE_CELL' IS 'ГРУППА'
    RETURNING CLOB
) RETURNING CLOB) AS CELLS
FROM (
        SELECT aid.WMS_LOCATION_ID, aid.INVENT_LOCATION_ID, ai3.AX_ID, sum(ai2.POSTED_QTY + ai2.RECEIVED - ai2.DEDUCTED + ai2.REGISTERED - ai2.PICKED) AS TOTAL
        FROM APP_ITEMS ai
        JOIN APP_ITEMSSUM ai2 ON ai2.ITEM_ID = ai.ID
        JOIN APP_ITEMSDIM aid ON aid.id = ai2.DIM_ID
        JOIN APP_MERCHLOCATIONSDIVISION am ON am.INVENT_LOCATION_ID = aid.INVENT_LOCATION_ID 
        JOIN APP_ITEMSVIEW ai3 ON ai.VIEW_ID = ai3.ID
        WHERE (LOWER(ai3.AX_ID) LIKE :BASE_TYPE
            OR LOWER(ai3.AX_ID) = 'кулла')
        AND ai2.CLOSED = 0
        AND am.DIVISION_ID = :DIV_ID
        GROUP BY aid.WMS_LOCATION_ID, aid.INVENT_LOCATION_ID, ai3.AX_ID
    ) inv_loc
JOIN APP_WMSLOCATIONS aw ON inv_loc.WMS_LOCATION_ID = aw.ID
                        AND inv_loc.INVENT_LOCATION_ID = aw.INVENT_LOCATION_ID 
WHERE aw.AX_ID LIKE :AX_ID
AND inv_loc.TOTAL >= 1
AND NOT EXISTS
    (SELECT * FROM MERCH_MERCHWMSDATA mmd
    WHERE mmd.WMS_ID = aw.ID)
-------
SELECT 
    JSON_ARRAYAGG(JSON_OBJECT(  
        QUALITY_TABLE.*,
        'PATH' IS (ad."PATH" || ad.FILE_NAME || '.' ||  ad.FILE_TYPE)
    RETURNING CLOB)RETURNING CLOB)	
FROM (SELECT 
    sum(ai5.POSTED_QTY + ai5.RECEIVED - ai5.DEDUCTED + ai5.REGISTERED - ai5.PICKED) AS TOTAL,
    table_item_spec.ID,
    table_item_spec.ARTICLE, 
    table_item_spec.NAME,
    table_item_spec.AX_REC_ID
    FROM (
    SELECT 
        ai4.ARTICLE,
        ai4.NAME,
        ai4.ID,
        ai4.AX_REC_ID
    FROM APP_ITEMS ais
    JOIN
        (SELECT
            aw.ID,
            sum(as2.POSTED_QTY + as2.RECEIVED - as2.DEDUCTED + as2.REGISTERED - as2.PICKED) AS TOTAL,
            as2.ITEM_ID 
        FROM APP_WMSLOCATIONS aw 
        JOIN APP_ITEMSDIM ai ON ai.WMS_LOCATION_ID = aw.ID
        JOIN APP_ITEMSSUM as2 ON ai.id = as2.DIM_ID
        JOIN APP_MERCHLOCATIONSDIVISION am3 ON am3.INVENT_LOCATION_ID = aw.INVENT_LOCATION_ID
        WHERE aw.ID = :ID
        AND am3.DIVISION_ID = :DIVISION_ID
        GROUP BY aw.id, as2.item_id
        ) se_it
    JOIN APP_ITEMSBOMVERSION ai2 ON ai2.ITEM_ID = se_it.ITEM_ID
    JOIN APP_ITEMSBOMLIST ai3 ON ai3.BOM_VERSION_ID = ai2.ID 
    JOIN APP_ITEMS ai4 ON ai4.ID = ai3.ITEM_ID 
    ON se_it.item_id = ais.ID and se_it.TOTAL > 0
) table_item_spec
JOIN  APP_ITEMSSUM ai5 ON ai5.ITEM_ID = table_item_spec.id
GROUP BY ai5.ITEM_ID, table_item_spec.ID, table_item_spec.ARTICLE, table_item_spec.NAME, table_item_spec.AX_REC_ID) QUALITY_TABLE
LEFT JOIN  APP_DOCUMENTS ad ON ad.DOC_TYPE = 'Picture_Z' AND ad.REF_REC_ID = QUALITY_TABLE.AX_REC_ID
where QUALITY_TABLE.TOTAL>0
-------

INSERT INTO MERCH_MERCHFLOORSPLANS(WMS_AX_ID_STR, URL, "DATE")
VALUES(:AX_ID, :FILE_NAME , CURRENT_DATE)

-------
SELECT 
    JSON_ARRAYAGG(JSON_OBJECT(
    ais.AX_ID,
    ais.NAME,
    se_it.TOTAL,
    se_it.ITEM_ID,
    se_it.ID
    ) RETURNING CLOB)
FROM APP_ITEMS ais
JOIN
    (SELECT
        aw.ID,
        (as2.POSTED_QTY + as2.RECEIVED - as2.DEDUCTED + as2.REGISTERED - as2.PICKED) AS TOTAL,
        as2.ITEM_ID 
    FROM APP_WMSLOCATIONS aw 
    JOIN APP_ITEMSDIM ai ON ai.WMS_LOCATION_ID = aw.ID
    JOIN APP_ITEMSSUM as2 ON aw.id = as2.DIM_ID
    JOIN APP_MERCHLOCATIONSDIVISION am3 ON am3.INVENT_LOCATION_ID = aw.INVENT_LOCATION_ID
    WHERE aw.ID = :ID
    AND am3.DIVISION_ID = :DIVISION_ID
    ) se_it
ON se_it.item_id = ais.ID


SELECT JSON_ARRAYAGG(JSON_OBJECT(*) RETURNING CLOB) FROM
    (SELECT mm.WMS_AX_ID_STR, mm.DIVISION_ID, mm.NAME, (
        SELECT JSON_ARRAYAGG(JSON_OBJECT(
                ITEMS.ID, 
                ITEMS.AX_ID,
                ITEMS.TOTAL
                RETURNING CLOB
                ) RETURNING CLOB) 
            FROM(SELECT 
                ai.ID,
                aw2.AX_ID,
                substr(aw2.AX_ID, 0, INSTR(aw2.AX_ID, '-') - 1) AS WMS_AX_ID_STR_REPL,
                (ai2.POSTED_QTY + ai2.RECEIVED - ai2.DEDUCTED + ai2.REGISTERED - ai2.PICKED) AS TOTAL
            FROM
                (SELECT
                    APP_ITEMS.ID
                FROM APP_ITEMS  
                    """ + app_items + app_type + """
                ) ai
            JOIN APP_ITEMSSUM ai2 ON ai2.ITEM_ID = ai.ID
            JOIN APP_ITEMSDIM aid ON aid.ID = ai2.DIM_ID
            JOIN APP_WMSLOCATIONS aw2 ON aw2.ID = aid.WMS_LOCATION_ID
            
            ) ITEMS 
        WHERE ITEMS.WMS_AX_ID_STR_REPL = mm.WMS_AX_ID_STR
        """ + app_wms + """
        ) AS L_JSON 
    FROM MERCH_MERCHFLOORS mm) mer
WHERE mer.L_JSON IS NOT NULL


SELECT
    JSON_ARRAYAGG(JSON_OBJECT(
        'WMS_ID' IS aw.ID,
        aw.AX_ID,
        am3.DIVISION_ID
    RETURNING CLOB )RETURNING CLOB)
FROM APP_WMSLOCATIONS aw
JOIN APP_ITEMSDIM ai ON ai.WMS_LOCATION_ID = aw.ID
JOIN APP_MERCHLOCATIONSDIVISION am3 ON am3.INVENT_LOCATION_ID = aw.INVENT_LOCATION_ID
WHERE aw.AX_ID LIKE :AX_ID
AND am3.DIVISION_ID = :DIVISION_ID
------
-- без header
CREATE OR REPLACE PACKAGE BODY WEBDB.MERCH AS
	-- Package body
	-- 'menu', not in plan canvas
	 FUNCTION get_cells_array(req_id IN NUMBER) RETURN sys_refcursor 
		IS cur sys_refcursor;
		BEGIN
			OPEN cur FOR SELECT
					aw.ID, aw.AX_ID, aw.AX_REC_ID, aw.AX_MODDIF_DATE, aw.AX_CREATE_DATE,
					aw.INVENT_LOCATION_ID
				FROM
					 APP_WMSLOCATIONS aw
				JOIN APP_INVENTLOCATION ai2 ON
					aw.INVENT_LOCATION_ID = ai2.ID
				WHERE
--					WEB_SHOW = 1 AND 
--					ai2.AX_REC_ID = req_id
--				AND 
					aw.AX_ID LIKE 'vrkva-x17-00-%'
				AND NOT EXISTS 
					(SELECT * FROM MERCH_MERCHWMSDATA mmd
					WHERE mmd.WMS_ID = aw.ID)
--					AND aw.LOCATION_LEVEL = req_floor
				ORDER BY
					NAME;
				RETURN cur;
			END;
	
	-- array cells render canvas
	FUNCTION get_cells_array_in_canvas(req_id IN NUMBER) RETURN sys_refcursor 
		IS cur sys_refcursor;
		BEGIN
			OPEN cur FOR SELECT
					aw.ID, aw.AX_ID, aw.AX_REC_ID, aw.AX_MODDIF_DATE, aw.AX_CREATE_DATE,
					aw.INVENT_LOCATION_ID, mmd1.*
				FROM
					 APP_WMSLOCATIONS aw
				JOIN APP_INVENTLOCATION ai2 ON
					aw.INVENT_LOCATION_ID = ai2.ID
				JOIN MERCH_MERCHWMSDATA mmd1 ON 
					mmd1.WMS_ID =  aw.ID 
				WHERE
					aw.AX_ID LIKE 'vrkva-x17-00-%'
				AND EXISTS 
					(SELECT * FROM MERCH_MERCHWMSDATA mmd
					WHERE mmd.WMS_ID = aw.ID)
				--	AND aw.LOCATION_LEVEL = req_floor
				ORDER BY
					ai2.NAME;
				RETURN cur;
			END;
		
	FUNCTION get_menu_merch RETURN sys_refcursor 
		IS cur_menu sys_refcursor;	
		BEGIN
			OPEN cur_menu FOR 
				SELECT mm.* FROM MERCH_MERCHFLOORS mm 
				ORDER BY mm.NAME; 
			RETURN cur_menu;
		END;
	
	FUNCTION insert_array_wms_web_coord(json_req IN clob) RETURN sys_refcursor
		IS 
		cur_return_status sys_refcursor;
		/* 		  		  _json
		 *		 	         |
		 *					 V 
		 *	 keys   		 :	value
		 *	 NAME				VARCHAR2(50)		
		 *	 DESCRIPTION  		VARCHAR2(200)
		 *	 ANGLE				FLOAT
		 *	 X					FLOAT
		 *	 Y					FLOAT
		 *	 TYPE_PALL_ID		NUMBER(30) --APP_TYPE_MERCH_PALLETS.TYPE_PALL_ID
		 *	 AX_PALL_ID 		NUMBER(30) -- APP_PALLETS.ID
		 */
	
	BEGIN
		INSERT INTO WEBDB.MERCH_MERCHWMSDATA(NAME, DESCRIPTION, ANGLE, M_X, M_Y, TYPE_ID, WMS_ID, HEIGHT, WIDTH, DIVISION)  
		SELECT NAME, DESCRIPTION, ANGLE, M_X, M_Y, TYPE_ID, WMS_ID, HEIGHT, WIDTH, DIVISION 
		FROM json_table(json_req , '$[*]'
	        COLUMNS(
				NAME 			PATH '$.NAME',
				DESCRIPTION		PATH '$.DESCRIPTION',
				ANGLE			PATH '$.ANGLE',
				M_X 			PATH '$.M_X', 
				M_Y				PATH '$.M_Y',
				TYPE_ID			PATH '$.TYPE_ID',
				WMS_ID			PATH '$.WMS_ID',
				HEIGHT			PATH '$.HEIGHT',
				WIDTH			PATH '$.WIDTH',
				DIVISION		PATH '$.DIVISION'
	            ) 
	       );
	    OPEN cur_return_status FOR 
	    	SELECT 'OK' AS status FROM dual;
		RETURN cur_return_status ;
	 EXCEPTION WHEN OTHERS THEN
	 	OPEN cur_return_status FOR 
	    	SELECT 'ERROR_INSERT_JSON_PALL' AS status FROM dual;
		RETURN cur_return_status ;
	 END;
	
	
	
	FUNCTION get_cells_array_json(id_div IN NUMBER, ax_id_req IN varchar2) RETURN clob
		IS array_ret clob;
	BEGIN
		SELECT JSON_ARRAYAGG(json_o.ax_id RETURNING CLOB) INTO array_ret FROM (SELECT JSON_OBJECT(aw.*) AS ax_id FROM 
		(SELECT aw.ID, aw.AX_ID, aw.AX_REC_ID, aw.AX_MODDIF_DATE, aw.AX_CREATE_DATE,
			aw.INVENT_LOCATION_ID
		FROM APP_WMSLOCATIONS aw
		JOIN APP_MERCHLOCATIONSDIVISION am3 ON am3.INVENT_LOCATION_ID  = aw.INVENT_LOCATION_ID
		WHERE am3.DIVISION_ID = id_div
		AND aw.AX_ID LIKE '%'+ ax_id_req +'%') aw) json_o;
		RETURN array_ret; 
	END;
	
	
END MERCH;










SELECT  * FROM (
SELECT  a.obj_id,  b.item_id, a.MASTER_WMS_ID, aw.ID AS WMS_ID, sum(b.POSTED_QTY + b.RECEIVED - b.DEDUCTED + b.REGISTERED - b.PICKED) AS TOTAL
FROM (
	SELECT  * FROM (
		SELECT  a.item_id AS obj_id,  sum(b.POSTED_QTY + b.RECEIVED - b.DEDUCTED + b.REGISTERED - b.PICKED) AS TOTAL, d.ID  AS MASTER_WMS_ID		 
		FROM  (
			SELECT a.id AS item_id 
			FROM app_items  a
			JOIN  APP_ITEMSVIEW b ON b.id  = a.VIEW_ID  
			WHERE REGEXP_LIKE(b.AX_ID , '(^стенд)|(кулла)'  )  
			GROUP  BY  a.id  
		) a
		JOIN APP_ITEMSSUM b ON b.ITEM_ID = a.item_id
		JOIN APP_ITEMSDIM c ON c.id  = b.DIM_ID 
		JOIN APP_WMSLOCATIONS d ON d.ID  = c.WMS_LOCATION_ID 
		WHERE b.CLOSED  = 0
		GROUP BY  a.item_id, d.ID 
	) 
	WHERE total > 0 
) a 
JOIN APP_WMSLOCATIONS aw  ON aw.MASTER_WMS_ID  = a.MASTER_WMS_ID
JOIN APP_ITEMSDIM c ON c.WMS_LOCATION_ID  = aw.id 
JOIN APP_ITEMSSUM b ON b.dim_id = c.id
WHERE b.CLOSED  = 0
GROUP  BY a.obj_id,  b.item_id, a.MASTER_WMS_ID, aw.ID  	  
) 
WHERE TOTAL > 0 
--\AND  
--WMS_ID = 10474